package com.example.iosdeveloper.element.Models;

public class CustomerOrderHistory {

    private String itemName;
    private String itemDate;
    private int itemAmount;
    private String orderStatus;

    public CustomerOrderHistory(String itemName, String itemDate, int itemAmount, String orderStatus) {
        this.itemName = itemName;
        this.itemDate = itemDate;
        this.itemAmount = itemAmount;
        this.orderStatus = orderStatus;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDate() {
        return itemDate;
    }

    public void setItemDate(String itemDate) {
        this.itemDate = itemDate;
    }

    public int getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(int itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
