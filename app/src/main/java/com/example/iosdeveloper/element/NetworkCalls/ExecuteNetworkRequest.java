package com.example.iosdeveloper.element.NetworkCalls;

/**
 * Created by iosdeveloper on 5/22/18.
 */
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

public class ExecuteNetworkRequest {
    //SharedPreferences preferences;
    //String theAccessToken;
    public ExecuteNetworkRequest(){
        //preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        //theUserName = preferences.getString("username", "");
    }

    /**
     Perform a normal get request and returns the value or object of the request
     */
    public Object performHttpGetRequestWithResponse(String requestURL, String authorization) {
        OkHttpClient httpClient = new OkHttpClient();

        String url = requestURL;
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer "+authorization)
                .build();

        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    /**
     Perform a get querry request and returns the value or object of the request
     */
    public Object performHTTPQueryRequestWithResponse(String requestParam, String requestURL, String authorization) {
        //OkHttpClient httpClient = new OkHttpClient();
        OkHttpClient httpClient = new OkHttpClient().newBuilder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240,TimeUnit.SECONDS).writeTimeout(240,TimeUnit.SECONDS).build();
        String url = requestURL;

        Request request = new Request.Builder()
                .url(requestURL)
                .get()
                .addHeader("content-type", "application/json")
                //.addHeader("cache-control", "no-cache")
                //.addHeader("postman-token", "2f1c493e-2908-2e81-b0f9-8fd88859803a")
                .build();

        //Response response = client.newCall(request).execute();

        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return null;

        /**HttpUrl.Builder httpBuider = HttpUrl.parse(url).newBuilder();
         httpBuider.addQueryParameter("coupon", requestParam);
         Request request = new Request.Builder().url(httpBuider.build()).addHeader("Authorization", "Bearer "+authorization).build();

         Response response = null;
         try {
         response = httpClient.newCall(request).execute();
         return response.body().string();
         } catch (IOException e) {
         Log.e(TAG, "error in getting response get request with query string okhttp");
         }

         return null;**/
    }

    public interface Login{
        void onLogin(boolean login);
    }
    // Perform HTTP POST Request using multipart form data

    public Object performpHttpURLEncodedFromPostRequestWithResponse(String requestParam, String requestURl, String authorization) {
        //OkHttpClient httpClient = new OkHttpClient();
        OkHttpClient httpClient = new OkHttpClient().newBuilder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240,TimeUnit.SECONDS).writeTimeout(240,TimeUnit.SECONDS).build();
        String url = requestURl;
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, requestParam);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Authorization", "Bearer "+authorization)
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.e(TAG, "Got response from server using OkHttp url form encoded ");
                return response.body().string();
            }
            else
            {
                return response.body().string();
            }

        } catch (IOException e) {
            Log.e(TAG, "error in getting response post request okhttp");
        }
        return null;

    }

    // Performs HTTP psot request using JSON
    public Object performHTTPJSONPostRequestWithResponse(String requestURL, String jsonString, String authorization){

        final MediaType mediaType
                = MediaType.parse("application/json");

        OkHttpClient httpClient = new OkHttpClient().newBuilder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240,TimeUnit.SECONDS).writeTimeout(240,TimeUnit.SECONDS).build();
        String url = requestURL;
        String jsonStr = jsonString; //"{\"coupon\":\"upto 20% off\", \"selectedDate\" : \"20/11/2016\"}";

        //post json using okhttp
        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(mediaType, jsonStr))
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer "+authorization)
                .addHeader("Cache-Control", "no-cache")
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.e(TAG, "Got response from server for JSON post using OkHttp ");
                return response.body().string();
            }
            else
            {
                return response.body().string();
            }

        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    //Perform HTTP post request using multipart form

    public Object performHTTPMultipartPostRequestWithResponse(String requestURL, String authorization){
        OkHttpClient httpClient = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("purpose", "xyz store coupons")
                .addFormDataPart("xml", "xyz_store_coupons.xml",
                        RequestBody.create(MediaType.parse("application/xml"), new File("website/static/xyz_store_coupons.xml")))
                .build();

        Request request = new Request.Builder()
                .url(requestURL)
                .post(requestBody)
                .addHeader("Authorization", "Bearer "+authorization)
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.e(TAG, "Got response from server for multipart request using OkHttp ");
                return response.body().string();
            }
            else
            {
                return response.body().string();
            }

        } catch (IOException e) {
            Log.e(TAG, "error in getting response for multipart request okhttp");
        }
        return null;
    }



    // Performs HTTP put request using JSON
    public Object performHTTPJSONPutRequestWithResponse(String requestURL, String jsonString, String authorization){

        final MediaType mediaType
                = MediaType.parse("application/json");

        RequestBody body = RequestBody.create(mediaType, jsonString);

        OkHttpClient httpClient = new OkHttpClient().newBuilder().connectTimeout(240, TimeUnit.SECONDS)
                .readTimeout(240,TimeUnit.SECONDS).writeTimeout(240,TimeUnit.SECONDS).build();
        String url = requestURL;
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer "+authorization)
                .addHeader("Cache-Control", "no-cache")
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.e(TAG, "Got response from server for JSON post using OkHttp ");
                return response.body().string();
            }
            else
            {
                return response.body().string();
            }

        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

}
