package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForUtilities;
import com.example.iosdeveloper.element.Models.CustomPickerModel;
import com.example.iosdeveloper.element.Models.WalletHistorySalesManModel;
import com.example.iosdeveloper.element.R;

import java.util.List;

public class CustomPickerAdapter  extends RecyclerView.Adapter<CustomPickerAdapter.MyViewHolderForCustomPicker>{

    private Context mContext;
    private List<CustomPickerModel> mData;
    private SelectionCallBackForUtilities selectionCallBack;

    /**public CustomPickerAdapter (SelectionCallBackForUtilities event)
    {
        selectionCallBack = event;
    }**/


    public CustomPickerAdapter(Context mContext, List<CustomPickerModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.selectionCallBack = (SelectionCallBackForUtilities) mContext;
    }


    @NonNull
    @Override
    public CustomPickerAdapter.MyViewHolderForCustomPicker onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.recycler_custom_picker,parent,false);
        return new CustomPickerAdapter.MyViewHolderForCustomPicker(view);
    }


    @Override
    public void onBindViewHolder(@NonNull CustomPickerAdapter.MyViewHolderForCustomPicker holder, final int position) {

        holder.itemName.setText(mData.get(position).getItemName());
        holder.itemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.i("Selected Value Routes", theSelectedValue+" "+theValueId);
                selectionCallBack.selectionHasBeenDone(mData.get(position));

            }
        });



    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForCustomPicker extends RecyclerView.ViewHolder{

        TextView itemName;
        View containerView;

        public MyViewHolderForCustomPicker(View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.custom_itemName);
            containerView = itemView.findViewById(R.id.custom_picker_innerview);


        }
    }


}
