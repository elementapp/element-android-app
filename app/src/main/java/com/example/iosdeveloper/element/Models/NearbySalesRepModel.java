package com.example.iosdeveloper.element.Models;

public class NearbySalesRepModel {
    private String nearbyrepName;
    private int nearbyRepID;
    private int nearbyRepDistance;
    private double theLat;
    private  double theLong;

    public NearbySalesRepModel(String nearbyrepName, int nearbyRepID, int nearbyRepDistance, double theLat, double theLong) {
        this.nearbyrepName = nearbyrepName;
        this.nearbyRepID = nearbyRepID;
        this.nearbyRepDistance = nearbyRepDistance;
        this.theLat=theLat;
        this.theLong = theLong;
    }

    public String getNearbyrepName() {
        return nearbyrepName;
    }

    public int getNearbyRepID() {
        return nearbyRepID;
    }

    public int getNearbyRepDistance() {
        return nearbyRepDistance;
    }

    public void setNearbyrepName(String nearbyrepName) {
        this.nearbyrepName = nearbyrepName;
    }

    public void setNearbyRepID(int nearbyRepID) {
        this.nearbyRepID = nearbyRepID;
    }

    public void setNearbyRepDistance(int nearbyRepDistance) {
        this.nearbyRepDistance = nearbyRepDistance;
    }

    public double getTheLat() {
        return theLat;
    }

    public void setTheLat(double theLat) {
        this.theLat = theLat;
    }

    public double getTheLong() {
        return theLong;
    }

    public void setTheLong(double theLong) {
        this.theLong = theLong;
    }
}
