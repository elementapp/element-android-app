package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.iosdeveloper.element.Adapters.CustomerOrderHistoryAdapter;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Models.CustomerOrderHistory;
import com.example.iosdeveloper.element.Models.ProductModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Customer_Invoice_Orders extends AppCompatActivity implements NetworkCallBacHAndler {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    TextView theUserName;
    static List<CustomerOrderHistory> lstOrderHistory;
    static RecyclerView myrv;
    static RecyclerView myrvForOrderHistory;
    static TextView noInvoiceLabel;
    static TextView noOrderLabel;
    boolean isGetUsersOrder;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer__invoice__orders);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theUserName = (TextView) findViewById(R.id.profileName);
        theUserName.setText(preferences.getString("username", ""));

        /**lstOrderHistory.add(new CustomerOrderHistory("Paracetamol Gel","27th July 2019",3500));
        lstOrderHistory.add(new CustomerOrderHistory("Atesonate","19th June 2012",8500));
        lstOrderHistory.add(new CustomerOrderHistory("Pyramax 344","11th September 2014",14000));
        lstOrderHistory.add(new CustomerOrderHistory("Paracetamol","17th July 2005",800));**/
        //setUpRecyclerViewForCustomerInvoice();
        getUsersOrderHistory();

    }

    void getUsersOrderHistory()
    {
        progressOverlay.setVisibility(View.VISIBLE);
        isGetUsersOrder = true;
        String theUrl = Constants.baseUrl + Constants.getUsersOrder.replace("{userId}",String.valueOf(preferences.getString("userid", "")));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        } else
        {
            if(isGetUsersOrder) {
                isGetUsersOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        JSONArray theProducts = obj.getJSONArray("message");
                        if (theProducts.length() > 0) {
                            lstOrderHistory = new ArrayList<>();
                            progressOverlay.setVisibility(View.GONE);
                            for (int i = 0; i < theProducts.length(); i++) {
                                JSONObject theOrderItem = theProducts.getJSONObject(i);
                                int thePrice = theOrderItem.getInt("totalProductPrice");
                                String theName = "Order:"+String.valueOf(theOrderItem.getInt("orderId"));
                                String theOrderstatus = theOrderItem.getString("orderStatus");
                                String theOrderDate = theOrderItem.getString("dateCreated");
                                lstOrderHistory.add(new CustomerOrderHistory(theName,theOrderDate,thePrice,theOrderstatus));
                            }
                            setUpRecyclerViewForCustomerInvoice(false);
                            setUpRecyclerViewForCustomerOrderHistory(true);
                        }
                        else {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    ("Order History currently not available.", this);
                        }
                    }
                    else {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }

                } catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Order History currently not available.", this);
                }
            }

        }

    }




   public void setUpRecyclerViewForCustomerInvoice(boolean isOrderHistory)
    {
        CustomerOrderHistoryAdapter myAdapter = new CustomerOrderHistoryAdapter(this, lstOrderHistory,isOrderHistory);
        myrv.setLayoutManager(new GridLayoutManager(this, 1));
        myrv.setAdapter(myAdapter);
    }


    public void setUpRecyclerViewForCustomerOrderHistory(boolean isOrderHistory)
    {
        CustomerOrderHistoryAdapter myAdapter = new CustomerOrderHistoryAdapter(this, lstOrderHistory,isOrderHistory);
        myrvForOrderHistory.setLayoutManager(new GridLayoutManager(this, 1));
        myrvForOrderHistory.setAdapter(myAdapter);
    }


    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer__invoice__orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_customer__invoice__orders, container, false);
            myrv = (RecyclerView) rootView.findViewById(R.id.recyclerview_customer_invoice);
            noInvoiceLabel = (TextView) rootView.findViewById(R.id.noInvoice);
            //new Customer_Invoice_Orders().setUpRecyclerViewForCustomerInvoice();
            return rootView;
        }
    }


    public static class SecondViewHolderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public SecondViewHolderFragment() {
        }

        public static SecondViewHolderFragment newInstance(int sectionNumber) {
            SecondViewHolderFragment fragment = new SecondViewHolderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_all_customer_orders, container, false);
            myrvForOrderHistory = (RecyclerView) rootView.findViewById(R.id.recyclerview_customer_orders);
            noOrderLabel = (TextView) rootView.findViewById(R.id.noOrders);
            return rootView;
        }
    }




    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment theSelectedPosition;
            if(position == 0)
            {
                theSelectedPosition = Customer_Invoice_Orders.PlaceholderFragment.newInstance(position);
            }
            else
            {
                theSelectedPosition = Customer_Invoice_Orders.SecondViewHolderFragment.newInstance(position);
            }
            return theSelectedPosition;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }
    }
}
