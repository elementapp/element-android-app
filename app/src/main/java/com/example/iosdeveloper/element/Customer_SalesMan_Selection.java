package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.iosdeveloper.element.Adapters.CatalogAdapters;
import com.example.iosdeveloper.element.Adapters.MySalesRepAdapter;
import com.example.iosdeveloper.element.Adapters.NearbySalesRepAdapter;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForDeleteAndEditDetails;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.CustomerModel;
import com.example.iosdeveloper.element.Models.MySalesRepModel;
import com.example.iosdeveloper.element.Models.NearbySalesRepModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Customer_SalesMan_Selection extends AppCompatActivity implements NetworkCallBacHAndler, SelectionCallBackForDeleteAndEditDetails {
    List<MySalesRepModel> lstMySalesRep;
    List<NearbySalesRepModel> lstNearbySalesRep;
    RecyclerView rcvMySalesRep;
    RecyclerView rcvNearbySalesRep;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    boolean isGetMySalesMen;
    boolean isGetNearbySalesman;
    boolean isCreateOrder;
    int theOrderId;
    Intent theUtilityIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer__sales_man__selection);
        rcvMySalesRep = (RecyclerView) findViewById(R.id.recycler_salesman_mylist);
        theUtilityIntent = getIntent();
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        rcvNearbySalesRep = (RecyclerView) findViewById(R.id.recycler_salesman_nearbyList);
        lstNearbySalesRep = new ArrayList<>();

        /**lstMySalesRep.add(new MySalesRepModel("Jonathan Simpson", "SALES REP ID: 9888123"));
        lstMySalesRep.add(new MySalesRepModel("Anthonio Bandaras", "SALES REP ID: 9456043"));
        lstMySalesRep.add(new MySalesRepModel("Julia Arobot", "SALES REP ID: 5698103"));**/
        getMySalesMen();

        /**lstNearbySalesRep.add(new NearbySalesRepModel("Donaldson Mayweather", "SALES REP ID: 9456043",
                "4km",0.0000,0.0000));
        lstNearbySalesRep.add(new NearbySalesRepModel("Cristiano Ronaldo", "SALES REP ID: 56709243",
                "6km",0.00000,0.0000));
        lstNearbySalesRep.add(new NearbySalesRepModel("Benjamin Franklin", "SALES REP ID: 52308443",
                "9km",0.0000,0.00000));
        lstNearbySalesRep.add(new NearbySalesRepModel("Mohammed Sallah", "SALES REP ID: 566092353",
                "13km",0.00000,0.0000));**/
        //setUpRecyclerForSalesMenNearby();
    }

    void getMySalesMen()
    {
        isGetMySalesMen = true;
        progressOverlay.setVisibility(View.VISIBLE);
        String theUrl = Constants.baseUrl + Constants.getUsersByType.replace("{userType}","1");
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }

    void getNearbySalesMen()
    {
        double theLatitude = Double.longBitsToDouble(preferences.getLong("latitude", Double.doubleToLongBits(0.000)));
        double theLongitude = Double.longBitsToDouble(preferences.getLong("longitude", Double.doubleToLongBits(0.000)));
        Log.i("The Values", "Latitude: "+ theLatitude+ " and longitude: "+theLongitude);
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            //locationDetailsHasBeenSent = true;
            isGetNearbySalesman = true;
            parameters.put("latitude", theLatitude);
            parameters.put("applicationUserId", preferences.getString("userid", ""));
            parameters.put("longitude", theLongitude);
            theJsonElement = parameters.toString();
            String theUrl = Constants.baseUrl + Constants.getSalesRepCloseToMyLocation;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            progressOverlay.setVisibility(View.VISIBLE);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, theAccessToken);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    void setUpRecyclerForSalesMenNearby()
    {
        NearbySalesRepAdapter nearbySalesRepAdapter = new NearbySalesRepAdapter(this, lstNearbySalesRep);
        rcvNearbySalesRep.setLayoutManager(new GridLayoutManager(this,1));
        rcvNearbySalesRep.setAdapter(nearbySalesRepAdapter);

    }

    void setUpRecyclerForMySalesRep()
    {
        MySalesRepAdapter mySalesRepAdapter = new MySalesRepAdapter(this, lstMySalesRep);
        rcvMySalesRep.setLayoutManager(new GridLayoutManager(this,1));
        rcvMySalesRep.setAdapter(mySalesRepAdapter);
    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            if(isGetMySalesMen)
            {
                isGetMySalesMen = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        JSONArray theManufacturers = obj.getJSONArray("message");
                        if(theManufacturers.length()>0)
                        {
                            lstMySalesRep = new ArrayList<>();
                            for (int i = 0; i < theManufacturers.length(); i++) {
                                JSONObject theItem = theManufacturers.getJSONObject(i);
                                String name = theItem.getString("userName");
                                String phoneNumber = theItem.getString("phoneNumber");
                                String email = theItem.getString("email");
                                int theId = theItem.getInt("salesRepId");
                                double theLat = theItem.getDouble("latitude");
                                double theLong = theItem.getDouble("longitude");
                                lstMySalesRep.add(new MySalesRepModel(name,phoneNumber,theId,theLat,theLong,email));
                            }
                            setUpRecyclerForMySalesRep();
                            getNearbySalesMen();
                        }
                        else {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    ("Sales Men currently not available. Check back later", this);
                        }
                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
            else if(isCreateOrder)
            {
                isCreateOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        Intent theSelection = new Intent(Customer_SalesMan_Selection.this, TransactionStatus.class);
                        theSelection.putExtra("labelValue", "Order Created");
                        theSelection.putExtra("descriptionValue", obj.getString("message"));
                        startActivity(theSelection);

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }

            else if(isGetNearbySalesman)
            {
                isGetNearbySalesman = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        JSONArray theManufacturers = obj.getJSONArray("message");
                        if(theManufacturers.length()>0)
                        {
                            lstNearbySalesRep = new ArrayList<>();
                            for (int i = 0; i < theManufacturers.length(); i++) {
                                JSONObject theItem = theManufacturers.getJSONObject(i);
                                String name = theItem.getString("userName");
                                String phoneNumber = theItem.getString("phoneNumber");
                                String email = theItem.getString("email");
                                int theId = theItem.getInt("salesRepId");
                                int theDistanceApart = theItem.getInt("distance");
                                double theLat = theItem.getDouble("latitude");
                                double theLong = theItem.getDouble("longitude");
                                lstNearbySalesRep.add(new NearbySalesRepModel(name,theId,theDistanceApart,theLat,theLong));
                            }
                            setUpRecyclerForSalesMenNearby();
                        }
                        else {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    ("Sales Men currently not available. Check back later", this);
                        }
                        /**Intent theSelection = new Intent(Customer_SalesMan_Selection.this, TransactionStatus.class);
                        theSelection.putExtra("labelValue", "Order Created");
                        theSelection.putExtra("descriptionValue", obj.getString("message"));
                        startActivity(theSelection);**/

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
        }

    }


    public void selectCustomerAndCreateOrder(Object theSelection)
    {
        final MySalesRepModel theSalesRep = (MySalesRepModel) theSelection;
        SalesRepDetails.theSalesRep = theSalesRep;
        Intent salesReps = new Intent(Customer_SalesMan_Selection.this, SalesRepDetails.class);
        salesReps.putExtra("salesrepID", theSalesRep.getRepIdReal());
        salesReps.putExtra("orderid", theOrderId);
        this.startActivity(salesReps);
    }

    public void editSelectedCustomerDetails(Object theSelection, int thePosition)
    {

    }
    public void deleteSelectedCustomerDetails(Object theSelection, int thePosition)
    {


    }
    public void selectNearbySalesRepAndCreateOrder(Object theSelection)
    {
        final NearbySalesRepModel theSalesRep = (NearbySalesRepModel) theSelection;
        CloseSalesRepDetails.theSalesRep = theSalesRep;
        Intent salesReps = new Intent(Customer_SalesMan_Selection.this, CloseSalesRepDetails.class);
        salesReps.putExtra("salesrepID", theSalesRep.getNearbyRepID());
        salesReps.putExtra("orderid", theOrderId);
        this.startActivity(salesReps);
    }

}
