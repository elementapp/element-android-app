package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.iosdeveloper.element.Models.WalletHistorySalesManModel;
import com.example.iosdeveloper.element.R;

import java.util.List;

public class WalletHistorySalesManAdapter extends RecyclerView.Adapter<WalletHistorySalesManAdapter.MyViewHolderForWalletHistorySalesMan> {

    private Context mContext;
    private List<WalletHistorySalesManModel> mData;


    public WalletHistorySalesManAdapter(Context mContext, List<WalletHistorySalesManModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }


    @NonNull
    @Override
    public WalletHistorySalesManAdapter.MyViewHolderForWalletHistorySalesMan onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.recycler_salesman_history,parent,false);
        return new WalletHistorySalesManAdapter.MyViewHolderForWalletHistorySalesMan(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletHistorySalesManAdapter.MyViewHolderForWalletHistorySalesMan holder, final int position) {

        holder.itemName.setText(mData.get(position).getItemName());
        holder.itemDate.setText(mData.get(position).getItemDate());
        holder.itemAmount.setText(mData.get(position).getItemAmount());
        int theType = (mData.get(position).getTransactionType());
        if(theType == 1)
        {
         holder.theTransType.setImageResource(R.drawable.up_arrow);
        }
        else
        {
            holder.theTransType.setImageResource(R.drawable.down_arrow);
        }


    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForWalletHistorySalesMan extends RecyclerView.ViewHolder{

        TextView itemName;
        TextView itemDate;
        TextView itemAmount;
        View containerView;
        ImageView theTransType;

        public MyViewHolderForWalletHistorySalesMan(View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            itemDate = (TextView) itemView.findViewById(R.id.datePurchased);
            itemAmount = (TextView) itemView.findViewById(R.id.amount);
            theTransType = (ImageView) itemView.findViewById(R.id.transactionType);
            containerView = itemView.findViewById(R.id.salesman_history);


        }
    }
}
