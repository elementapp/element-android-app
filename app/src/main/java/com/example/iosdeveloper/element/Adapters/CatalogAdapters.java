package com.example.iosdeveloper.element.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.R;
import com.example.iosdeveloper.element.SelectedProducts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CatalogAdapters extends RecyclerView.Adapter<CatalogAdapters.MyViewHolderForCatalogue>{

    private Context mContext;
    private List<CatlogueModel> mData;


    public CatalogAdapters(Context mContext, List<CatlogueModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }


    @NonNull
    @Override
    public CatalogAdapters.MyViewHolderForCatalogue onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_catalogue,parent,false);
        return new CatalogAdapters.MyViewHolderForCatalogue(view);
    }


    @Override
    public void onBindViewHolder(@NonNull CatalogAdapters.MyViewHolderForCatalogue holder, final int position) {

        holder.catalogue_drugName.setText(mData.get(position).getTheDrugName());

        holder.shopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //selectionCallBack.denyTripForSelectedTrip(mData.get(position), position);
                Intent intent = new Intent(mContext,SelectedProducts.class);
                intent.putExtra("categoryid",mData.get(position).getTheCatId());
                mContext.startActivity(intent);

            }
        });

    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForCatalogue extends RecyclerView.ViewHolder{

        TextView catalogue_drugName;
        CardView cardView;
        Button shopNow;

        public MyViewHolderForCatalogue(View itemView) {
            super(itemView);

            catalogue_drugName = (TextView) itemView.findViewById(R.id.drugName);
            cardView = (CardView) itemView.findViewById(R.id.cardview_catalogue_id);
            shopNow = (Button) itemView.findViewById(R.id.shopNow);


        }
    }

}
