package com.example.iosdeveloper.element.Models;

public class CustomPickerModel {
    private String itemName;
    private String itemId;

    public CustomPickerModel(String itemName, String itemId) {
        this.itemName = itemName;
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
