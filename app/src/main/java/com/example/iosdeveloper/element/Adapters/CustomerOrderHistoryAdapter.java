package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iosdeveloper.element.Models.CustomerOrderHistory;
import com.example.iosdeveloper.element.Models.WalletHistorySalesManModel;
import com.example.iosdeveloper.element.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CustomerOrderHistoryAdapter  extends RecyclerView.Adapter<CustomerOrderHistoryAdapter.MyViewHolderForCustomerOrderHistory> {


    private Context mContext;
    private List<CustomerOrderHistory> mData;
    private boolean isOrderHistory;

    public CustomerOrderHistoryAdapter(Context mContext, List<CustomerOrderHistory> mData, boolean isOrderHistory) {
        this.mContext = mContext;
        this.mData = mData;
        this.isOrderHistory =isOrderHistory;
    }

    @NonNull
    @Override
    public CustomerOrderHistoryAdapter.MyViewHolderForCustomerOrderHistory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.recycler_customer_orderhistory,parent,false);
        return new CustomerOrderHistoryAdapter.MyViewHolderForCustomerOrderHistory(view);
    }





    @Override
    public void onBindViewHolder(@NonNull CustomerOrderHistoryAdapter.MyViewHolderForCustomerOrderHistory holder, final int position) {

        holder.itemName.setText(mData.get(position).getItemName());
        holder.itemDate.setText(mData.get(position).getItemDate());
        holder.itemAmount.setText("N"+String.valueOf(mData.get(position).getItemAmount()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS");
        SimpleDateFormat dateFormatTwo = new SimpleDateFormat("dd MMM, yyyy");
        try{
            Date convertedDate = dateFormat.parse(mData.get(position).getItemDate());
            long timeInMillis = convertedDate.getTime();
            holder.itemDate.setText(dateFormatTwo.format(timeInMillis));

        }catch (ParseException e){
            Log.v("ParseException",""+e);
        }

        holder.payAction.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Log.i("We want To Pay", "Paying now as you require");
                                                }
                                            }
        );
        if(isOrderHistory)
        {
            holder.payAction.setVisibility(View.GONE);
        }
        else
        {
            if(mData.get(position).getOrderStatus().equals("Booked")) {
                holder.payAction.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.payAction.setVisibility(View.GONE);
            }
        }

    }





    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForCustomerOrderHistory extends RecyclerView.ViewHolder{

        TextView itemName;
        TextView itemDate;
        TextView itemAmount;
        TextView payAction;
        View containerView;
        View theFrameLayout;
        //ImageView theTransType;

        public MyViewHolderForCustomerOrderHistory(View itemView) {
            super(itemView);

            itemName = (TextView) itemView.findViewById(R.id.itemName);
            itemDate = (TextView) itemView.findViewById(R.id.datePurchased);
            itemAmount = (TextView) itemView.findViewById(R.id.amount);
            containerView = itemView.findViewById(R.id.customer_history);
            theFrameLayout = itemView.findViewById(R.id.marginLineIndicator);
            payAction = (TextView) itemView.findViewById(R.id.paymentAction);


        }
    }
}
