package com.example.iosdeveloper.element;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class DashBoard_Customer extends AppCompatActivity {
    TextView theUsername;
    SharedPreferences preferences;
    String theAccessToken;
    final int PERMISSION_ACCESS_FINE_LOCATION = 0;
    final int PERMISSION_ACCESS_LOCATION_PROMPT = 0;
    LocationManager locationManager;
    double longitudeBest, latitudeBest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board__customer);
        theUsername = (TextView) findViewById(R.id.dashboardCustomerNameLabel);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theUsername.setText("Hello "+ preferences.getString("username", ""));
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    protected void onResume()
    {
        setUpLocationUpdateForUser();
        //locationDetailsHasBeenSent=false;
        super.onResume();
    }

    void setUpLocationUpdateForUser()
    {
        if(!checkLocation())
            return;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED /**|| ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
         != PackageManager.PERMISSION_GRANTED**/)
        {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_FINE_LOCATION);
        }
        else {

            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            String provider = locationManager.getBestProvider(criteria, true);
            if(provider != null) {
                locationManager.requestLocationUpdates(provider, 2 * 60 * 1000, 10, locationListenerBest);
                //Toast.makeText(this, "Best Provider is " + provider, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "allow this app access your updated location")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }



    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeBest =  location.getLongitude();
            latitudeBest =  location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //longitudeValueBest.setText(longitudeBest + "");
                    //latitudeValueBest.setText(latitudeBest + "");
                    //Toast.makeText(DashBoard_Customer.this, "Best Provider update", Toast.LENGTH_SHORT).show();
                    Log.i("The Details", "This is the longitude "+longitudeBest+" and latitude "+latitudeBest);
                    saveUseraLatandLongToPreference();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    void saveUseraLatandLongToPreference()
    {
        SharedPreferences preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("latitude",  Double.doubleToRawLongBits(latitudeBest));
        editor.putLong("longitude",  Double.doubleToRawLongBits(longitudeBest));
        editor.apply();
    }


    public void openTheSalesManScreen(View view){
        Intent catalogue = new Intent(DashBoard_Customer.this, Customer_SalesMan_Selection.class);
        startActivity(catalogue);
    }

    public void openMyOrdersScreen(View view){
        Intent invoice = new Intent(DashBoard_Customer.this, Customer_Invoice_Orders.class);
        startActivity(invoice);

    }

    public void openCatalogueScreen(View view){
        Intent catalogue = new Intent(DashBoard_Customer.this, Catalogue.class);
        startActivity(catalogue);

    }

    public void logOutUser(View view){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Loging out limits your use of this application");

        alertDialogBuilder.setTitle("Do you wish to Log Out ?");

        alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //theActivity.finish();
            }
        });

        alertDialogBuilder.setPositiveButton("Proceed",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                startActivity( intent );
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
