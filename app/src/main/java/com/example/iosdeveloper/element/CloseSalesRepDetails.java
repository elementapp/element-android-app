package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.widget.TextView;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Models.NearbySalesRepModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class CloseSalesRepDetails extends AppCompatActivity implements OnMapReadyCallback,NetworkCallBacHAndler {
    private GoogleMap mMap;
    static View progressOverlay;
    int theOrderId;
    int theSalesRepID;
    Intent theUtilityIntent;
    SharedPreferences preferences;
    String theAccessToken;
    TextView thename,theId,theDistance;
    public static NearbySalesRepModel theSalesRep;
    boolean isGetMySalesMenDetails;
    private static final int COLOR_APPGREEEN_ARGB = 0xff3cb878;
    private static final int COLOR_WHITE_ARGB = 0xffffffff;
    private static final int POLYLINE_STROKE_WIDTH_PX = 15;
    private static final int POLYGON_STROKE_WIDTH_PX = 12;
    private static final int PATTERN_DASH_LENGTH_PX = 20;
    private static final int PATTERN_GAP_LENGTH_PX = 20;
    private static final PatternItem DOT = new Dot();
    private static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    private static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    // Create a stroke pattern of a gap followed by a dash.
    private static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);

    // Create a stroke pattern of a gap followed by a dot.
    private static final List<PatternItem> PATTERN_POLYGON_GAMMA = Arrays.asList(GAP, DOT);

    // Create a stroke pattern of a dot followed by a gap, a dash, and another gap.
    private static final List<PatternItem> PATTERN_POLYGON_BETA =
            Arrays.asList(DOT, GAP, DASH, GAP);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_sales_rep_details);
        theUtilityIntent = getIntent();
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        theSalesRepID = theUtilityIntent.getIntExtra("salesrepID",0000);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        thename = (TextView) findViewById(R.id.nearestrepname);
        theId = (TextView) findViewById(R.id.nearestrepid);
        theDistance = (TextView) findViewById(R.id.nearestrepdistance);

        String name = theSalesRep.getNearbyrepName();
        if (name == null || name.isEmpty()
                || name.equalsIgnoreCase("null")) {
            thename.setText("Name Not Available");
        }
        else
        {
            thename.setText(name);
        }
        theId.setText("Sales Rep ID: "+String.valueOf(theSalesRep.getNearbyRepID()));
        theDistance.setText(String.valueOf(theSalesRep.getNearbyRepDistance()) +" km");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        double theLatitude = Double.longBitsToDouble(preferences.getLong("latitude", Double.doubleToLongBits(0.000)));
        double theLongitude = Double.longBitsToDouble(preferences.getLong("longitude", Double.doubleToLongBits(0.000)));
        Log.i("The Values", "Latitude: "+ theLatitude+ " and longitude: "+theLongitude);

        //double theSalesRepLatitude = 7.000000;
        //double theSalesRepLongitude = 3.800000;

        double theSalesRepLatitude = theSalesRep.getTheLat();
        double theSalesRepLongitude = theSalesRep.getTheLong();


        /**Polyline polyline1 = mMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(theLatitude, theLongitude),
                        new LatLng(7.000000, 3.800000)));



        polyline1.setEndCap(new RoundCap());
        polyline1.setWidth(POLYLINE_STROKE_WIDTH_PX);
        polyline1.setColor(COLOR_BLACK_ARGB);
        polyline1.setJointType(JointType.ROUND);**/
        //polyline1.

        Polygon polygon = googleMap.addPolygon(new PolygonOptions()
                .clickable(true)
                .add(
                        new LatLng(theLatitude, theLongitude),
                        new LatLng(theSalesRepLatitude, theSalesRepLongitude)));



        //Add Customer Lat and Long
        LatLng customerLocation = new LatLng(theLatitude, theLongitude);
        mMap.addMarker(new MarkerOptions().position(customerLocation).title("You")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker)));

        //Add Sales Rep Lat and Long
        LatLng salesRepLocation = new LatLng(theSalesRepLatitude, theSalesRepLongitude);
        mMap.addMarker(new MarkerOptions().position(salesRepLocation).title("Sales Rep")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng((theLatitude+theSalesRepLatitude)/2,
                (theLongitude+theSalesRepLongitude)/2), 10));

        styleThePolyGon(polygon);

    }


    void styleThePolyGon(Polygon polygon)
    {
        List<PatternItem> pattern = null;
        int strokeColor = COLOR_APPGREEEN_ARGB;
        int fillColor = COLOR_WHITE_ARGB;

        pattern = PATTERN_POLYGON_GAMMA;
        polygon.setStrokePattern(pattern);
        polygon.setStrokeWidth(POLYGON_STROKE_WIDTH_PX);
        polygon.setStrokeColor(strokeColor);
        polygon.setFillColor(fillColor);
    }



    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void placeRequest(View view)
    {
        Intent paymentMethod = new Intent(CloseSalesRepDetails.this, PaymentMethods.class);
        paymentMethod.putExtra("salesrepID", theSalesRepID);
        paymentMethod.putExtra("orderid", theOrderId);
        this.startActivity(paymentMethod);
    }

    void getSalesRepDetails()
    {
        progressOverlay.setVisibility(View.VISIBLE);
        isGetMySalesMenDetails = true;
        String theUrl = Constants.baseUrl + Constants.getSalesRepDetails.replace("{id}",String.valueOf(theSalesRepID));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }

    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            if(isGetMySalesMenDetails)
            {
                isGetMySalesMenDetails = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        JSONObject theItem = obj.getJSONObject("message");
                        String name = theItem.getString("salesRepName");
                        int salesRepIDValue = theItem.getInt("salesRepId");
                        String emailValue = theItem.getString("emailAddress");
                        String phoneNumber = theItem.getString("phoneNumber");
                        /**if (name == null || name.isEmpty()
                                || name.equalsIgnoreCase("null")) {
                            theName.setText("Not Available");
                        }
                        else
                        {
                            theName.setText(name);
                        }
                        if (emailValue == null || emailValue.isEmpty()
                                || emailValue.equalsIgnoreCase("null")) {
                            email.setText("Not Available");
                        }
                        else
                        {
                            email.setText(emailValue);
                        }
                        salesRepId.setText("Sales Rep ID: "+salesRepIDValue);
                        if (phoneNumber == null || phoneNumber.isEmpty()
                                || phoneNumber.equalsIgnoreCase("null")) {
                            phoneNumberText.setText("Not Available");
                        }
                        else
                        {
                            phoneNumberText.setText(phoneNumber);
                        }**/

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }

        }

    }


}
