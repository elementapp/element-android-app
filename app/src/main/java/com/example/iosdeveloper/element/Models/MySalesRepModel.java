package com.example.iosdeveloper.element.Models;

public class MySalesRepModel {
    private String repName;
    private String repId;
    private int repIdReal;
    private double theLat;
    private  double theLong;
    private String repemail;

    public MySalesRepModel(String repName, String repId, int repIdReal, double theLat, double theLong, String repemail) {
        this.repName = repName;
        this.repId = repId;
        this.repIdReal = repIdReal;
        this.theLat = theLat;
        this.theLong = theLong;
        this.repemail = repemail;
    }

    public String getRepName() {
        return repName;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public int getRepIdReal() {
        return repIdReal;
    }

    public void setRepIdReal(int repIdReal) {
        this.repIdReal = repIdReal;
    }

    public double getTheLat() {
        return theLat;
    }

    public void setTheLat(double theLat) {
        this.theLat = theLat;
    }

    public double getTheLong() {
        return theLong;
    }

    public void setTheLong(double theLong) {
        this.theLong = theLong;
    }

    public String getRepemail() {
        return repemail;
    }

    public void setRepemail(String repemail) {
        this.repemail = repemail;
    }
}
