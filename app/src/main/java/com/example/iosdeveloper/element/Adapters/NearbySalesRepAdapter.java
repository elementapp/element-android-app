package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForDeleteAndEditDetails;
import com.example.iosdeveloper.element.Models.MySalesRepModel;
import com.example.iosdeveloper.element.Models.NearbySalesRepModel;
import com.example.iosdeveloper.element.R;
import com.example.iosdeveloper.element.SalesRepDetails;

import java.util.List;

public class NearbySalesRepAdapter extends RecyclerView.Adapter<NearbySalesRepAdapter.MyViewHolderForNearbyRep>{

    private Context mContext;
    private List<NearbySalesRepModel> mData;
    private SelectionCallBackForDeleteAndEditDetails selectionCallBack;


    public NearbySalesRepAdapter(Context mContext, List<NearbySalesRepModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.selectionCallBack = (SelectionCallBackForDeleteAndEditDetails) mContext;
    }

    @NonNull
    @Override
    public NearbySalesRepAdapter.MyViewHolderForNearbyRep onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_closest_salesrep,parent,false);
        return new NearbySalesRepAdapter.MyViewHolderForNearbyRep(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final NearbySalesRepAdapter.MyViewHolderForNearbyRep holder, final int position) {

        String name = mData.get(position).getNearbyrepName();
        if (name == null || name.isEmpty()
                || name.equalsIgnoreCase("null")) {
            holder.repName.setText("Name Not Available");
        }
        else
        {
            holder.repName.setText(name);
        }
        holder.repID.setText("Sales Rep ID: "+String.valueOf(mData.get(position).getNearbyRepID()));
        holder.distance.setText(String.valueOf(mData.get(position).getNearbyRepDistance()) +" km");

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("see Nearby", "We want to view the nearby sales rep");
                selectionCallBack.selectNearbySalesRepAndCreateOrder(mData.get(position));
                //Intent salesReps = new Intent(mContext, SalesRepDetails.class);
                //mContext.startActivity(salesReps);


            }
        });

    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForNearbyRep extends RecyclerView.ViewHolder{

        TextView repName;
        TextView repID;
        TextView distance;
        CardView cardView;

        public MyViewHolderForNearbyRep(View itemView) {
            super(itemView);

            repID = (TextView) itemView.findViewById(R.id.nearestrepid);
            repName = (TextView) itemView.findViewById(R.id.nearestrepname);
            distance = (TextView) itemView.findViewById(R.id.nearestrepdistance);
            cardView = (CardView) itemView.findViewById(R.id.cardview_nearbysalesrep);


        }
    }


}
