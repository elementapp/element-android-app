package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.iosdeveloper.element.Adapters.CatalogAdapters;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Catalogue extends AppCompatActivity implements NetworkCallBacHAndler {
    List<CatlogueModel> lstCatalogs;
    RecyclerView myrv;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    private SwipeRefreshLayout swipeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        myrv = (RecyclerView) findViewById(R.id.recycler_catalogue);
        getProductCategories();
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //your method to refresh content
                Log.i("refresh now", "we are swipping to refresh");
                getProductCategories();
            }
        });
        /**lstCatalogs.add(new CatlogueModel("Anti-Viral Drugs"));
        lstCatalogs.add(new CatlogueModel("Antibiotics"));
        lstCatalogs.add(new CatlogueModel("Painkillers"));
        lstCatalogs.add(new CatlogueModel("Skin Care and Dermatology"));
        lstCatalogs.add(new CatlogueModel("Food Suppliment"));
        lstCatalogs.add(new CatlogueModel("Sexual Enhancment"));
        setUpRecycler();**/

    }

    void getProductCategories()
    {
        progressOverlay.setVisibility(View.VISIBLE);
        String theUrl = Constants.baseUrl + Constants.getProductCategories;
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }



    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    void setUpRecycler()
    {
        CatalogAdapters myAdapter = new CatalogAdapters(this, lstCatalogs);
        myrv.setLayoutManager(new GridLayoutManager(this,1));
        myrv.setAdapter(myAdapter);
    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        if(swipeLayout.isRefreshing()) {
            swipeLayout.setRefreshing(false);
        }
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        } else {
            try {
                JSONObject obj = new JSONObject(res);
                String theResponseCode = obj.getString("status");
                if (theResponseCode.equals("success")) {
                    JSONArray theCategories = obj.getJSONArray("message");
                    if (theCategories.length() > 0) {
                        progressOverlay.setVisibility(View.GONE);
                        lstCatalogs = new ArrayList<>();
                        for (int i = 0; i < theCategories.length(); i++) {
                            JSONObject theCategoryItem = theCategories.getJSONObject(i);
                            String name = theCategoryItem.getString("productCategoryName");
                            int theCategoryId = theCategoryItem.getInt("productCategoryId");
                            lstCatalogs.add(new CatlogueModel(name, theCategoryId));
                        }
                        setUpRecycler();
                    } else {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                ("Categories currently not available. Check back later", this);
                    }

                }
                else
                {
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            (obj.getString("message"), this);
                }

                }
             catch (Throwable t) {
                Log.i("Result For Array", t.getLocalizedMessage());
                progressOverlay.setVisibility(View.GONE);
                CommonFunctions.showSimpleAlertDialogue
                        ("Categories currently not available, please try later", this);
            }

        }

    }
}
