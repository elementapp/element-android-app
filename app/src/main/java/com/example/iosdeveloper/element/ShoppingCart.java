package com.example.iosdeveloper.element;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.iosdeveloper.element.Adapters.CartAdapter;
import com.example.iosdeveloper.element.Adapters.CatalogAdapters;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Interfaces.CartModificationInterface;
import com.example.iosdeveloper.element.Models.CartModel;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.ProductModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart extends AppCompatActivity implements NetworkCallBacHAndler, CartModificationInterface {
    RecyclerView myrv;
    List<ProductModel> lstProductsForCart;
    TextView theTotalAmount;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    boolean isGetUsersCart;
    boolean isUpdateUsersCart;
    Intent theUtilityIntent;
    int theOrderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        myrv = (RecyclerView) findViewById(R.id.recycler_cart_recycler);
        theTotalAmount = (TextView) findViewById(R.id.totalAmount);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        //setUpCartUi();
        /**lstCartItems = new ArrayList<>();
        lstCartItems.add(new CartModel("Claricell Kids", "REF. 13060273 - WINNIE", "N7,000"));
        lstCartItems.add(new CartModel("Lutogel 100mg", "REF. 13064568 - JOHNSON", "N5,700"));
        lstCartItems.add(new CartModel("Paracetamol 100mg", "REF. 14539568 - SAMUEL", "N15,500"));**/
        getUserCart();
        //setUpRecycler();
    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void checkOutNow(View view)
    {
        //Intent transaction = new Intent(ShoppingCart.this, TransactionStatus.class);
        //startActivity(transaction);

        int usertype = preferences.getInt("usertype", 0000);

        if(usertype == 1)
        {
            Intent theSelection = new Intent(ShoppingCart.this, SalesMan_Customers.class);
            theSelection.putExtra("labelValue", "Select Customer");
            theSelection.putExtra("orderid", theOrderId);
            startActivity(theSelection);
        }
        else
        {
            Intent theSelection = new Intent(ShoppingCart.this, Customer_SalesMan_Selection.class);
            theSelection.putExtra("orderid", theOrderId);
            startActivity(theSelection);
        }

    }

    void getUserCart()
    {
        progressOverlay.setVisibility(View.VISIBLE);
        isGetUsersCart = true;
        String theUrl = Constants.baseUrl + Constants.getUsersCart.replace("{userId}",String.valueOf(preferences.getString("userid", "")));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);
    }

    void setUpRecycler()
    {
        CartAdapter myAdapter = new CartAdapter(this, lstProductsForCart);
        myrv.setLayoutManager(new GridLayoutManager(this,1));
        myrv.setAdapter(myAdapter);
    }



    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        } else
        {
            if(isGetUsersCart) {
                isGetUsersCart = false;
                try {
                 JSONObject obj = new JSONObject(res);
                 String theResponseCode = obj.getString("status");
                 if (theResponseCode.equals("success")) {
                 JSONArray theProducts = obj.getJSONArray("message");
                 if (theProducts.length() > 0) {
                 progressOverlay.setVisibility(View.GONE);
                 String theTotalPrice = "N"+ theProducts.getJSONObject(0).getInt("totalProductPrice");
                 theOrderId = theProducts.getJSONObject(0).getInt("orderId");
                 theTotalAmount.setText(theTotalPrice);
                     lstProductsForCart = new ArrayList<>();
                     for (int i = 0; i < theProducts.length(); i++) {
                         JSONObject theCategoryItem = theProducts.getJSONObject(i);
                         String name = theCategoryItem.getString("productName");
                         int theProductId = theCategoryItem.getInt("productId");
                         int theProductQuantity = theCategoryItem.getInt("productQuantity");
                         String productImage = theCategoryItem.getString("productImage");
                         if (productImage == null || productImage.isEmpty()
                                 || productImage.equalsIgnoreCase("null")
                                 || productImage.equals("string")
                                 || productImage.contains("~")) {
                             productImage = "no image";
                         } else {
                             productImage = Constants.imageBaseUrl + productImage;
                         }
                         String theProductRef = theCategoryItem.getString("referenceNumber");
                         if (theProductRef == null || theProductRef.isEmpty()
                                 || theProductRef.equalsIgnoreCase("null")) {
                             theProductRef = "000000";
                         }
                         int theAmount = theCategoryItem.getInt("productPrice");
                         lstProductsForCart.add(new ProductModel(productImage, name, "REF. 345672" /**+ String.valueOf(theProductRef)**/,
                                 theAmount, theProductId,theProductQuantity));


                     }
                     if(isUpdateUsersCart)
                     {
                         isUpdateUsersCart = false;
                         CommonFunctions.showSimpleAlertDialogue
                                 ("Cart Item modified successfully.", this);
                     }
                     setUpRecycler();
                 }
                 else {
                 progressOverlay.setVisibility(View.GONE);
                 CommonFunctions.showSimpleAlertDialogue
                 ("Products currently not available in your cart.", this);
                 }
                 }
                 else {
                 progressOverlay.setVisibility(View.GONE);
                 CommonFunctions.showSimpleAlertDialogue
                 (obj.getString("message"), this);
                 }

                 } catch (Throwable t) {
                 Log.i("Result For Array", t.getLocalizedMessage());
                 progressOverlay.setVisibility(View.GONE);
                 CommonFunctions.showSimpleAlertDialogue
                 ("Products currently not available in your cart", this);
                 }
            }

        }

    }


    public void editUsersCart(final Object theSelection, int theProductQuantity)
    {
        Log.i("Edit Cart", "This is the new Quantity "+theProductQuantity);
        final ProductModel theProduct = (ProductModel) theSelection;
        if(theProductQuantity>0) {
            isGetUsersCart = true;
            isUpdateUsersCart = true;
            String theJsonElement = "";
            JSONObject parameters = new JSONObject();
            try {
                parameters.put("productId", theProduct.getProductID());
                parameters.put("userId", preferences.getString("userid", ""));
                parameters.put("productQuantity", theProductQuantity);
                theJsonElement = parameters.toString();
                progressOverlay.setVisibility(View.VISIBLE);
                String theUrl = Constants.baseUrl + Constants.updateUsersCartItem;
                Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
                new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            Log.i("Delete", "You want to delete the product");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Do you wish to delete this item from your cart?");
            alertDialogBuilder.setTitle("Confirm Action");

            alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //theActivity.finish();
                }
            });

            alertDialogBuilder.setPositiveButton("Yes, Delete",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeItemFromUsersCart(theProduct);
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    void removeItemFromUsersCart(ProductModel theSelection)
    {
        progressOverlay.setVisibility(View.VISIBLE);
        isGetUsersCart = true;
        String theUrl = Constants.baseUrl + Constants.deleteUsersCartItem.replace("{userId}",String.valueOf(preferences.getString("userid", "")))
                .replace("{productId}",String.valueOf(theSelection.getProductID()));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);
    }


}
