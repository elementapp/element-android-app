package com.example.iosdeveloper.element.Models;

public class ProductModel {

    private String productimageURL;
    private String productName;
    private String productRef;
    private int productAmount;
    private int productID;
    private int productQuantity;

    public ProductModel(String productimageURL, String productName, String productRef, int productAmount,int productID, int productQuantity) {
        this.productimageURL = productimageURL;
        this.productName = productName;
        this.productRef = productRef;
        this.productAmount = productAmount;
        this.productID = productID;
        this.productQuantity = productQuantity;
    }

    public String getProductimageURL() {
        return productimageURL;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductRef() {
        return productRef;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductimageURL(String productimageURL) {
        this.productimageURL = productimageURL;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }
}
