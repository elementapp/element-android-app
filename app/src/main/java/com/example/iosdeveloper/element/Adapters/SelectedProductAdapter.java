package com.example.iosdeveloper.element.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForUtilities;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.ProductModel;
import com.example.iosdeveloper.element.R;
import com.example.iosdeveloper.element.SelectedProducts;
import com.example.iosdeveloper.element.ShoppingCart;

import java.util.List;

public class SelectedProductAdapter extends RecyclerView.Adapter<SelectedProductAdapter.MyViewHolderForSelectedProducts> {

    private Context mContext;
    private List<ProductModel> mData;
    private SelectionCallBackForUtilities selectionCallBack;

    public SelectedProductAdapter(Context mContext, List<ProductModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.selectionCallBack = (SelectionCallBackForUtilities) mContext;
    }


    @NonNull
    @Override
    public SelectedProductAdapter.MyViewHolderForSelectedProducts onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_selected_product,parent,false);
        return new SelectedProductAdapter.MyViewHolderForSelectedProducts(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedProductAdapter.MyViewHolderForSelectedProducts holder, final int position) {

        holder.productName.setText(mData.get(position).getProductName());
        holder.productPrice.setText("N"+mData.get(position).getProductAmount());
        holder.productRef.setText(mData.get(position).getProductRef());
        if(mData.get(position).getProductimageURL().equals("no image"))
        {
            holder.productImage.setImageResource(R.drawable.product_selected);
        }
        else {
            Glide.with(mContext).load(mData.get(position).getProductimageURL()).into(holder.productImage);
        }

        holder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(mContext,ShoppingCart.class);
                //mContext.startActivity(intent);


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setMessage("Do you wish to add this item to your cart?");
                alertDialogBuilder.setTitle("Confirm Action");

                alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //theActivity.finish();
                    }
                });

                alertDialogBuilder.setPositiveButton("Add To Cart",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectionCallBack.selectionHasBeenDone(mData.get(position));
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForSelectedProducts extends RecyclerView.ViewHolder{

        TextView productName;
        TextView productRef;
        TextView productPrice;
        ImageView productImage;
        ImageView addToCart;
        CardView cardView;


        public MyViewHolderForSelectedProducts(View itemView) {
            super(itemView);

            productName = (TextView) itemView.findViewById(R.id.productName);
            productRef = (TextView) itemView.findViewById(R.id.productReference);
            productPrice = (TextView) itemView.findViewById(R.id.productAmount);
            productImage = (ImageView) itemView.findViewById(R.id.productImage);
            addToCart = (ImageView) itemView.findViewById(R.id.addToCart);
            cardView = (CardView) itemView.findViewById(R.id.cardview_selected_product);


        }
    }
}
