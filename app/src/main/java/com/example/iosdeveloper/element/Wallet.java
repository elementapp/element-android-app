package com.example.iosdeveloper.element;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.iosdeveloper.element.Adapters.WalletHistorySalesManAdapter;
import com.example.iosdeveloper.element.Models.WalletHistorySalesManModel;

import java.util.ArrayList;
import java.util.List;

public class Wallet extends AppCompatActivity {
    static RecyclerView myrv;
    static List<WalletHistorySalesManModel> lstSalesManHistory;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setSelectedTabIndicatorHeight(9);
        tabLayout.setTabGravity(Gravity.CENTER);
        lstSalesManHistory = new ArrayList<>();
        Log.i("Again","We are in the first view now ohh");
        //setUpSalesManWalletHistory();

        /**FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });**/

    }

    public void setUpSalesManWalletHistory()
    {
        Log.i("Again","We are in the third view now ohh");
        lstSalesManHistory.add(new WalletHistorySalesManModel("Drugs ordered for Mary Ann","Today 13:45"
                ,"N45,700",0));
        lstSalesManHistory.add(new WalletHistorySalesManModel("GTBank Transfer POS","Friday 21:00"
                ,"N32,000",1));
        lstSalesManHistory.add(new WalletHistorySalesManModel("Drugs ordered for Cosby","Wednesday 18:32"
                ,"N15,800",1));
        lstSalesManHistory.add(new WalletHistorySalesManModel("Transfer to Greenwood Inc","Last week"
                ,"N65,700",0));
        lstSalesManHistory.add(new WalletHistorySalesManModel("Transfer to Trino Farm","Wednesday 18:19"
                ,"N80,700",1));

        WalletHistorySalesManAdapter myAdapter = new WalletHistorySalesManAdapter(this, lstSalesManHistory);
        myrv.setLayoutManager(new GridLayoutManager(this, 1));
        myrv.setAdapter(myAdapter);


    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wallet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_withdraw_salesman, container, false);
            return rootView;
        }
    }


    public static class SecondViewHolderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public SecondViewHolderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static SecondViewHolderFragment newInstance(int sectionNumber) {
            SecondViewHolderFragment fragment = new SecondViewHolderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_history_salesman, container, false);
            myrv = (RecyclerView) rootView.findViewById(R.id.recycler_salesman_history);
            Log.i("Again","We are in the second view now ohh");
            ((Wallet)getActivity()).setUpSalesManWalletHistory();
            //new Wallet().setUpSalesManWalletHistory();
            return rootView;
        }

        /**public void setUpSalesManWalletHistory()
        {
            lstSalesManHistory.add(new WalletHistorySalesManModel("Drugs ordered for Mary Ann","Today 13:45"
                    ,"N45,700",0));
            lstSalesManHistory.add(new WalletHistorySalesManModel("GTBank Transfer POS","Friday 21:00"
                    ,"N32,000",1));
            lstSalesManHistory.add(new WalletHistorySalesManModel("Drugs ordered for Cosby","Wednesday 18:32"
                    ,"N15,800",0));
            lstSalesManHistory.add(new WalletHistorySalesManModel("Transfer to Greenwood Inc","Last week"
                    ,"N65,700",0));

            WalletHistorySalesManAdapter myAdapter = new WalletHistorySalesManAdapter(this, lstSalesManHistory);
            myrv.setLayoutManager(new GridLayoutManager(this, 1));
            myrv.setAdapter(myAdapter);


        }**/

    }


    public static class ThirdViewHolderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public ThirdViewHolderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ThirdViewHolderFragment newInstance(int sectionNumber) {
            ThirdViewHolderFragment fragment = new ThirdViewHolderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_statistics_salesman, container, false);
            //myrv = (RecyclerView) rootView.findViewById(R.id.recyclerview_trip_history);
            //noVehicleLabel = (TextView) rootView.findViewById(R.id.noHistoryTextView);
            return rootView;
        }
    }




    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment theSelectedPosition;
            if(position == 0)
            {
                theSelectedPosition = PlaceholderFragment.newInstance(position);
            }
            else if(position == 1)
            {
                theSelectedPosition = SecondViewHolderFragment.newInstance(position);
            }
            else
            {
                theSelectedPosition = ThirdViewHolderFragment.newInstance(position);
            }
            return theSelectedPosition;
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
