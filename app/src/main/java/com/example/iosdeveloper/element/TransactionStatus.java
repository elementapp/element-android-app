package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TransactionStatus extends AppCompatActivity {
    TextView theStatusLabel;
    TextView theMessageLAbel;
    Intent theUtilityIntent;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        theUtilityIntent = getIntent();
        theStatusLabel = (TextView) findViewById(R.id.status);
        theMessageLAbel = (TextView) findViewById(R.id.statusMessage);
        theStatusLabel.setText(theUtilityIntent.getStringExtra("labelValue"));
        theMessageLAbel.setText(theUtilityIntent.getStringExtra("descriptionValue"));
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
    }


    public void goToDashBoard(View view)
    {
        int usertype = preferences.getInt("usertype", 0000);
        if(usertype == 1) {
            Intent intent = new Intent(getApplicationContext(), DashBoard.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity( intent );
        }
        if(usertype == 0) {
            Intent intent = new Intent(getApplicationContext(), DashBoard_Customer.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity( intent );
        }

    }
}
