package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.iosdeveloper.element.Adapters.CatalogAdapters;
import com.example.iosdeveloper.element.Adapters.CustomerAdapter;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForDeleteAndEditDetails;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.CustomerModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SalesMan_Customers extends AppCompatActivity implements SelectionCallBackForDeleteAndEditDetails, NetworkCallBacHAndler {
    List<CustomerModel> lstCustomers;
    RecyclerView myrv;
    Intent theUtilityIntent;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    int theOrderId;
    boolean isGetRetailers;
    boolean isCreateOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_man__customers);
        myrv = (RecyclerView) findViewById(R.id.recycler_customer);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theUtilityIntent = getIntent();
        TextView theHeader = (TextView) findViewById(R.id.headerTextSalesMan);
        theHeader.setText(theUtilityIntent.getStringExtra("labelValue"));
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        /**lstCustomers = new ArrayList<>();
        lstCustomers.add(new CustomerModel("MediPlus Nigeria","CUSTOMER ID: 9888123"));
        lstCustomers.add(new CustomerModel("Green Line Pharmarcy","CUSTOMER ID: 98887654"));
        lstCustomers.add(new CustomerModel("Flowwell Pharmarcy","CUSTOMER ID: 9450123"));
        lstCustomers.add(new CustomerModel("Airen Pharmarcy","CUSTOMER ID: 98856873"));
        lstCustomers.add(new CustomerModel("MediPlus Nigeria","CUSTOMER ID: 9888123"));
        lstCustomers.add(new CustomerModel("Greenwood Inc.","CUSTOMER ID: 54782056"));
        lstCustomers.add(new CustomerModel("Sales Agent Otta","CUSTOMER ID: 54786896"));
        lstCustomers.add(new CustomerModel("Trinity Drugstore","CUSTOMER ID: 54732566"));
        setUpRecycler();**/
        getRetailers();


    }

    void getRetailers()
    {
        isGetRetailers = true;
        progressOverlay.setVisibility(View.VISIBLE);
        String theUrl = Constants.baseUrl + Constants.getUsersByType.replace("{userType}","0");
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }

    void setUpRecycler()
    {
        CustomerAdapter myAdapter = new CustomerAdapter(this, lstCustomers);
        myrv.setLayoutManager(new GridLayoutManager(this,1));
        myrv.setAdapter(myAdapter);
    }


    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }


    public void addANewCustomer(View view)
    {
        Log.i("Add customer", "We want to add a customer");
        Intent newUser = new Intent(SalesMan_Customers.this, SignUPActivity.class);
        startActivity(newUser);

    }



   public void editSelectedCustomerDetails(Object theSelection, int thePosition)
    {

    }
    public void deleteSelectedCustomerDetails(Object theSelection, int thePosition)
    {
        lstCustomers.remove(theSelection);
        setUpRecycler();
    }

    public void selectNearbySalesRepAndCreateOrder(Object theSelection)
    {
    }

    public void selectCustomerAndCreateOrder(Object theSelection)
    {
        Log.i("Order", "We want to create order now");
        isCreateOrder = true;
        final CustomerModel theCustomer = (CustomerModel) theSelection;
        Intent paymentMethod = new Intent(SalesMan_Customers.this, PaymentMethods.class);
        paymentMethod.putExtra("salesrepID", theCustomer.getCustomerIdReal());
        paymentMethod.putExtra("orderid", theOrderId);
        this.startActivity(paymentMethod);
        /**String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("orderId", theOrderId);
            parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("retailerId",theCustomer.getCustomerIdReal());
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.placeOrderForProducts;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }**/

    }

    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
           if(isGetRetailers)
           {
               isGetRetailers = false;
               try {
                   JSONObject obj = new JSONObject(res);
                   String theResponseCode = obj.getString("status");
                   if (theResponseCode.equals("success")) {
                       progressOverlay.setVisibility(View.GONE);
                       JSONArray theManufacturers = obj.getJSONArray("message");
                       if(theManufacturers.length()>0)
                       {
                           lstCustomers = new ArrayList<>();
                           for (int i = 0; i < theManufacturers.length(); i++) {
                               JSONObject theItem = theManufacturers.getJSONObject(i);
                               String name = theItem.getString("userName");
                               String phoneNumber = theItem.getString("phoneNumber");
                               int theId = theItem.getInt("retailerId");
                               lstCustomers.add(new CustomerModel(name,phoneNumber,theId));
                           }
                           setUpRecycler();
                       }
                       else {
                           progressOverlay.setVisibility(View.GONE);
                           CommonFunctions.showSimpleAlertDialogue
                                   ("Retailers currently not available. Check back later", this);
                       }
                   }
                   else
                   {
                       progressOverlay.setVisibility(View.GONE);
                       CommonFunctions.showSimpleAlertDialogue
                               (obj.getString("message"), this);
                   }
               }
               catch (Throwable t) {
                   Log.i("Result For Array", t.getLocalizedMessage());
                   progressOverlay.setVisibility(View.GONE);
                   CommonFunctions.showSimpleAlertDialogue
                           ("Error processing your request. Try again at some other time", this);
               }

           }
           else if(isCreateOrder)
           {
               isCreateOrder = false;
               try {
                   JSONObject obj = new JSONObject(res);
                   String theResponseCode = obj.getString("status");
                   if (theResponseCode.equals("success")) {
                       progressOverlay.setVisibility(View.GONE);
                       Intent theSelection = new Intent(SalesMan_Customers.this, TransactionStatus.class);
                       theSelection.putExtra("labelValue", "Order Created");
                       theSelection.putExtra("descriptionValue", obj.getString("message"));
                       startActivity(theSelection);

                   }
                   else
                   {
                       progressOverlay.setVisibility(View.GONE);
                       CommonFunctions.showSimpleAlertDialogue
                               (obj.getString("message"), this);
                   }
               }
               catch (Throwable t) {
                   Log.i("Result For Array", t.getLocalizedMessage());
                   progressOverlay.setVisibility(View.GONE);
                   CommonFunctions.showSimpleAlertDialogue
                           ("Error processing your request. Try again at some other time", this);
               }

           }
        }

    }
}
