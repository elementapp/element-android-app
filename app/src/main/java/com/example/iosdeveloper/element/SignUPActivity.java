package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Models.CustomPickerModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.paystack.android.PaystackSdk;

public class SignUPActivity extends AppCompatActivity implements NetworkCallBacHAndler {
    EditText theName;
    EditText theAddress;
    EditText theEmail;
    EditText thePassword;
    EditText retypePassword;
    EditText thePhoneNumber;
    Button theSignUpButton;
    Button userType;
    static View progressOverlay;
    private  MakeNetwotkRequest makeNetwotkRequest;
    List<JSONObject> lstItems;
    public static String theSelectedUserType ="Select User Type";
    public static String theSelectedUserTypeId;
    Boolean isSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        theName = (EditText) findViewById(R.id.nameField);
        userType = (Button) findViewById(R.id.usertypeField);
        theEmail = (EditText) findViewById(R.id.emailField);
        thePassword = (EditText) findViewById(R.id.passwordField);
        retypePassword = (EditText) findViewById(R.id.retypepasswordField);
        thePhoneNumber = (EditText) findViewById(R.id.phoneNumberField);
        progressOverlay = findViewById(R.id.progress_overlay);
        theSignUpButton = (Button) findViewById(R.id.signUpButton);
        isSignIn = false;
        initialisePaystack();

    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    protected void onResume()
    {
        userType.setText(theSelectedUserType);
        super.onResume();
    }

    void initialisePaystack()
    {
        PaystackSdk.initialize(getApplicationContext());
    }

    public static void setPaystackKey(String publicKey) {
        PaystackSdk.setPublicKey(publicKey);
    }



    public void performSignUpOperation(View view) {
        if (theName.getText().toString().equals("")) {
            CommonFunctions.showSimpleAlertDialogue("Enter a name", this);
        } else if (userType.getText().toString().equals("Select User Type")) {
            CommonFunctions.showSimpleAlertDialogue("Select a user type", this);
        } else if (theEmail.getText().toString().equals("")) {
            CommonFunctions.showSimpleAlertDialogue("Enter an email address", this);
        } else if (thePassword.getText().toString().equals("")) {
            CommonFunctions.showSimpleAlertDialogue("Enter a desired password", this);
        } else if (retypePassword.getText().toString().equals("")) {
            CommonFunctions.showSimpleAlertDialogue("Validate your password", this);
        } else if (!thePassword.getText().toString().equals(retypePassword.getText().toString())) {
            CommonFunctions.showSimpleAlertDialogue("Password mismatch. Verify both passwords are thesame", this);
        } else if (thePhoneNumber.getText().toString().equals("")) {
            CommonFunctions.showSimpleAlertDialogue("Enter your phone number", this);
        } else {

            /** "email": "string",
             "phoneNumber": "string",
             "userName": "string",
             "password": "string",
             "userType": 0  **/

            String theJsonElement = "";
            JSONObject parameters = new JSONObject();
            try {
                parameters.put("email", theEmail.getText().toString());
                parameters.put("phoneNumber", thePhoneNumber.getText().toString());
                parameters.put("userName", theName.getText().toString());
                parameters.put("password", thePassword.getText().toString());
                parameters.put("userType", theSelectedUserTypeId);
                theJsonElement = parameters.toString();
                progressOverlay.setVisibility(View.VISIBLE);
                theSignUpButton.setVisibility(View.GONE);
                String theUrl = Constants.baseUrl + Constants.signup;
                new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    void performSignInOnsuccessfullSignUp()
    {
        isSignIn = true;
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("phoneNumber", thePhoneNumber.getText().toString());
            parameters.put("password", thePassword.getText().toString());
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.signIn;
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void displayPickerForUserTypes(View view)
    {
        lstItems = new ArrayList<>();
        JSONObject parameters = new JSONObject();
        JSONObject parameters2 = new JSONObject();
        JSONObject parameters3 = new JSONObject();
        JSONObject parameters4 = new JSONObject();
        try {
            parameters.put("name", "Retailer");
            parameters.put("idValue", "0");
            lstItems.add(parameters);
            parameters2.put("name", "Sales Rep");
            parameters2.put("idValue", "1");
            lstItems.add(parameters2);
            parameters3.put("name", "Manufacturer");
            parameters3.put("idValue", "2");
            lstItems.add(parameters3);
            parameters4.put("name", "Employee");
            parameters4.put("idValue", "3");
            lstItems.add(parameters4);
            JSONArray theValueJson = new JSONArray(lstItems);
            Log.i("this is the JSOAN Array", theValueJson.toString());
            if (theValueJson.length() > 0) {
                Intent utility = new Intent(SignUPActivity.this, CustomPickerUtility.class);
                utility.putExtra("theutility", theValueJson.toString());
                utility.putExtra("label", "signup");
                startActivity(utility);
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



        public void networkCallHasBeenCompleted (Object theResult)
        {
            Log.i("Api Request Result", (String) theResult);
            String res = (String) theResult;
            if (theResult == "Error retrieving your response") {
                progressOverlay.setVisibility(View.GONE);
                theSignUpButton.setVisibility(View.VISIBLE);
                CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                        this);
            }
            else
            {
                if(isSignIn)
                {
                    isSignIn = false;
                    try {
                        JSONObject obj = new JSONObject(res);
                        String theResponseCode = obj.getString("status");
                        if (theResponseCode.equals("success")) {
                            progressOverlay.setVisibility(View.GONE);
                            String accessToken = obj.getString("token");
                            String userPhoneNumber = obj.getString("phoneNumber");
                            String userEmail = obj.getString("email");
                            String userName = obj.getString("userName");
                            int usertype = obj.getInt("userType");
                            String userId = obj.getString("userId");
                            SharedPreferences preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("userPhone", userPhoneNumber);
                            editor.putString("accesstoken",accessToken);
                            editor.putString("username",userName);
                            editor.putString("email", userEmail);
                            editor.putString("userid", userId);
                            editor.putInt("usertype",usertype);
                            editor.apply();

                            if(usertype == 1)
                            {
                                Intent dashBoard = new Intent(SignUPActivity.this, DashBoard.class);
                                startActivity(dashBoard);
                            }
                            else
                            {
                                Intent dashBoard = new Intent(SignUPActivity.this, DashBoard_Customer.class);
                                startActivity(dashBoard);
                            }


                        } else {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    (obj.getString("message"), this);

                        }

                    } catch (Throwable t) {
                        Log.i("Result For Array", t.getLocalizedMessage());
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                ("Sign in unsuccessfull, please try again later", this);
                    }
                }
                else
                    {
                    try {
                        JSONObject obj = new JSONObject(res);
                        String theResponseCode = obj.getString("status");
                        if (theResponseCode.equals("success")) {
                            progressOverlay.setVisibility(View.GONE);
                            performSignInOnsuccessfullSignUp();


                        } else {
                            progressOverlay.setVisibility(View.GONE);
                            theSignUpButton.setVisibility(View.VISIBLE);
                            CommonFunctions.showSimpleAlertDialogue
                                    (obj.getString("message"), this);

                        }

                    } catch (Throwable t) {
                        theSignUpButton.setVisibility(View.VISIBLE);
                        Log.i("Result For Array", t.getLocalizedMessage());
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                ("Sign up unsuccessfull, please try again later", this);
                    }
                }
            }

        }


}
