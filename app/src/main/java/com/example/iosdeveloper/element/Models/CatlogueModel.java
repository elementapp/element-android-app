package com.example.iosdeveloper.element.Models;

public class CatlogueModel {

    private String theDrugName;
    private int theCatId;

    public CatlogueModel(String theDrugName,int theCatId) {
        this.theDrugName = theDrugName;
        this.theCatId = theCatId;
    }

    public String getTheDrugName() {
        return theDrugName;
    }

    public void setTheDrugName(String theDrugName) {
        this.theDrugName = theDrugName;
    }

    public int getTheCatId() {
        return theCatId;
    }

    public void setTheCatId(int theCatId) {
        this.theCatId = theCatId;
    }
}
