package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentMethods extends AppCompatActivity implements NetworkCallBacHAndler {
    int theSelectedPaymentMethod;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    int theOrderId;
    int theSalesRepID;
    Intent theUtilityIntent;
    boolean isCreateOrder;
    RadioButton theCashPayment;
    RadioButton theCardPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_methods);
        theUtilityIntent = getIntent();
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        theSalesRepID = theUtilityIntent.getIntExtra("salesrepID",0000);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theCardPayment = (RadioButton) findViewById(R.id.cardpaymentSelection);
        theCashPayment = (RadioButton) findViewById(R.id.cashpaymentSelection);
    }


    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void cashPaymentClicked(View view)
    {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.cashpaymentSelection:
                if (checked) {
                    theSelectedPaymentMethod = 0;
                    theCardPayment.setChecked(false);
                    Log.i("Selected Payment", "Cash payment was selected");
                    //placeOrder();
                }
                    break;
            case R.id.cardpaymentSelection:
                if (checked) {
                    theSelectedPaymentMethod = 1;
                    theCashPayment.setChecked(false);
                    Log.i("Selected Payment", "Card payment was selected");
                }
                    break;
        }
    }

    void placeOrder()
    {
        isCreateOrder = true;
        Log.i("Log out", "We want to log out");
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("orderId", theOrderId);
            parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("salesRepId",theSalesRepID);
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.placeOrderForProducts;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            if(isCreateOrder)
            {
                isCreateOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        Intent theSelection = new Intent(PaymentMethods.this, TransactionStatus.class);
                        theSelection.putExtra("labelValue", "Order Created");
                        theSelection.putExtra("descriptionValue", "Order ID: "+obj.getString("message"));
                        startActivity(theSelection);

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
        }

    }


    public void placeRequest(View view)
    {
        if(theSelectedPaymentMethod == 0)
        {
            placeOrder();
        }
        else if (theSelectedPaymentMethod == 1)
        {
            Intent cardDetails = new Intent(PaymentMethods.this, CardDetails.class);
            cardDetails.putExtra("salesrepID", theSalesRepID);
            cardDetails.putExtra("orderid", theOrderId);
            this.startActivity(cardDetails);

        }
        else
        {
            CommonFunctions.showSimpleAlertDialogue("Select a payment method for this order",PaymentMethods.this);
        }

    }
}
