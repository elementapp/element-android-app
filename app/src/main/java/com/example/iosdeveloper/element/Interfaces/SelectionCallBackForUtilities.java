package com.example.iosdeveloper.element.Interfaces;

public interface SelectionCallBackForUtilities {

    void selectionHasBeenDone(Object theResult);

}
