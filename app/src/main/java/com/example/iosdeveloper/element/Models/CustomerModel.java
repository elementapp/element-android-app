package com.example.iosdeveloper.element.Models;

public class CustomerModel {

    private String customerName;
    private String customerId;
    private int customerIdReal;

    public CustomerModel(String customerName, String customerId, int customerIdReal) {
        this.customerName = customerName;
        this.customerId = customerId;
        this.customerIdReal = customerIdReal;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getCustomerIdReal() {
        return customerIdReal;
    }

    public void setCustomerIdReal(int customerIdReal) {
        this.customerIdReal = customerIdReal;
    }
}
