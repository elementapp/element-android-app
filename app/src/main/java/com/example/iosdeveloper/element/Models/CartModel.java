package com.example.iosdeveloper.element.Models;

public class CartModel {
    private String productName;
    private String productRef;
    private String productAmount;

    public CartModel(String productName, String productRef, String productAmount) {
        this.productName = productName;
        this.productRef = productRef;
        this.productAmount = productAmount;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductRef() {
        return productRef;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }
}
