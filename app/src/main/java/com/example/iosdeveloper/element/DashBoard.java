package com.example.iosdeveloper.element;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

public class DashBoard extends AppCompatActivity implements NetworkCallBacHAndler{
    TextView theUsername;
    SharedPreferences preferences;
    String theAccessToken;
    static View progressOverlay;
    final int PERMISSION_ACCESS_FINE_LOCATION = 0;
    final int PERMISSION_ACCESS_LOCATION_PROMPT = 0;
    LocationManager locationManager;
    double longitudeBest, latitudeBest;
    double longitudeGPS, latitudeGPS;
    double longitudeNetwork, latitudeNetwork;
    private LocationSettingsRequest.Builder builder;
    private LocationRequest mLocationRequest;
    boolean isUpdateLocation;
    boolean locationDetailsHasBeenSent;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        theUsername = (TextView) findViewById(R.id.dashboardNameLabel);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        progressOverlay = findViewById(R.id.progress_overlay);
        theUsername.setText("Hello "+ preferences.getString("username", ""));
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /**createLocation();
        buildLocationSettingsRequest();

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this)
                .checkLocationSettings(builder.build());

        result.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.v("Value 2", "Location is satisfied");
            }
        });

        result.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        Log.w("Value", "Location settings not satisfied, attempting resolution intent");
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(DashBoard.this, PERMISSION_ACCESS_LOCATION_PROMPT);
                        } catch (IntentSender.SendIntentException sendIntentException) {
                            Log.e("Value", "Unable to start resolution intent");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.w("Value", "Location settings not satisfied and can't be changed");
                        break;
                }
            }
        });**/

        //setUpLocationUpdateForUser();
    }

    protected void onResume()
    {
        setUpLocationUpdateForUser();
        locationDetailsHasBeenSent=false;
        super.onResume();
    }

    void setUpLocationUpdateForUser()
    {
        if(!checkLocation())
            return;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED /**|| ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED**/)
            {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                        PERMISSION_ACCESS_FINE_LOCATION);
            }
            else {

                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(Criteria.POWER_LOW);
                String provider = locationManager.getBestProvider(criteria, true);
                if(provider != null) {
                locationManager.requestLocationUpdates(provider, 2 * 60 * 1000, 10, locationListenerBest);
                //Toast.makeText(this, "Best Provider is " + provider, Toast.LENGTH_LONG).show();
            }
        }
    }



    /**protected void buildLocationSettingsRequest() {

        builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        builder.build();
    }

    protected void createLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }**/





    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "allow this app access your updated location")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }


    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }



    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeBest =  location.getLongitude();
            latitudeBest =  location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //longitudeValueBest.setText(longitudeBest + "");
                    //latitudeValueBest.setText(latitudeBest + "");
                    //Toast.makeText(DashBoard.this, "Best Provider update", Toast.LENGTH_SHORT).show();
                    Log.i("The Details", "This is the longitude "+longitudeBest+" and latitude "+latitudeBest);
                    sendUseraLatandLongToServer();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    void sendUseraLatandLongToServer()
    {
        //if(!locationDetailsHasBeenSent) {
            String theJsonElement = "";
            JSONObject parameters = new JSONObject();
            try {
                //locationDetailsHasBeenSent = true;
                isUpdateLocation = true;
                parameters.put("latitude", latitudeBest);
                parameters.put("applicationUserId", preferences.getString("userid", ""));
                parameters.put("longitude", longitudeBest);
                theJsonElement = parameters.toString();
                String theUrl = Constants.baseUrl + Constants.updateUsersLocation;
                Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
                new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, theAccessToken);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
       // }
    }



    public void networkCallHasBeenCompleted (Object theResult) {
        if(isUpdateLocation)
        {
         isUpdateLocation = false;
            Log.i("Api Request Result", (String) theResult);
            String res = (String) theResult;
        }

    }


   /**public void checkForUsersLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_FINE_LOCATION);
        }
    }**/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                    setUpLocationUpdateForUser();
                } else {
                    Toast.makeText(this, "Your location is needed to update the service!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }



    public void openCustomersScreen(View view){

        Intent catalogue = new Intent(DashBoard.this, SalesMan_Customers.class);
        startActivity(catalogue);


    }

    public void openWalletScreen(View view){

        Intent catalogue = new Intent(DashBoard.this, Wallet.class);
        startActivity(catalogue);

    }

    public void openCatalogueScreen(View view){
        Intent catalogue = new Intent(DashBoard.this, Catalogue.class);
        startActivity(catalogue);

    }

    public void logOutUser(View view){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Loging out limits your use of this application");

        alertDialogBuilder.setTitle("Do you wish to Log Out ?");

        alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //theActivity.finish();
            }
        });

        alertDialogBuilder.setPositiveButton("Proceed",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                startActivity( intent );
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void viewUserProfile(View view){

        Intent profile = new Intent(DashBoard.this, UserProfile.class);
        startActivity(profile);


    }
}
