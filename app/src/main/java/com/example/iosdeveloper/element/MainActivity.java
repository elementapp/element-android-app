package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONException;
import org.json.JSONObject;

import co.paystack.android.PaystackSdk;

public class MainActivity extends AppCompatActivity implements NetworkCallBacHAndler {
    EditText phoneNumberFields;
    EditText passwordFields;
    static View progressOverlay;
    private  MakeNetwotkRequest makeNetwotkRequest;
    Button theSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeFroLoginScreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phoneNumberFields = (EditText) findViewById(R.id.usernameField);
        passwordFields = (EditText) findViewById(R.id.passwordField);
        progressOverlay = findViewById(R.id.progress_overlay);
        theSignInButton = (Button) findViewById(R.id.loginbutton);
        PaystackSdk.initialize(getApplicationContext());
    }


    public void performSignInOperation(View view){
        if(phoneNumberFields.getText().toString().equals(""))
        {
            CommonFunctions.showSimpleAlertDialogue("Enter your registered phone number", this);
        }
        else if(passwordFields.getText().toString().equals(""))
        {
            CommonFunctions.showSimpleAlertDialogue("Enter your password", this);
        }
        else
        {
            String theJsonElement = "";
            JSONObject parameters = new JSONObject();
            try {
                parameters.put("phoneNumber", phoneNumberFields.getText().toString());
                parameters.put("password", passwordFields.getText().toString());
                theJsonElement = parameters.toString();
                progressOverlay.setVisibility(View.VISIBLE);
                theSignInButton.setVisibility(View.GONE);
                String theUrl = Constants.baseUrl + Constants.signIn;
                new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        //Intent dashBoard = new Intent(MainActivity.this, DashBoard.class);
        //startActivity(dashBoard);

    }

    public void moveToSignUpScreen(View view){
       Intent signUp = new Intent(MainActivity.this, SignUPActivity.class);
       startActivity(signUp);

    }


    public void networkCallHasBeenCompleted (Object theResult)
    {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            theSignInButton.setVisibility(View.VISIBLE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            try {
                JSONObject obj = new JSONObject(res);
                String theResponseCode = obj.getString("status");
                if (theResponseCode.equals("success")) {
                    progressOverlay.setVisibility(View.GONE);
                    String accessToken = obj.getString("token");
                    String userPhoneNumber = obj.getString("phoneNumber");
                    String userEmail = obj.getString("email");
                    String userName = obj.getString("userName");
                    int usertype = obj.getInt("userType");
                    String userId = obj.getString("userId");
                    SharedPreferences preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("userPhone", userPhoneNumber);
                    editor.putString("accesstoken",accessToken);
                    editor.putString("username",userName);
                    editor.putString("email", userEmail);
                    editor.putString("userid", userId);
                    editor.putInt("usertype",usertype);
                    editor.apply();

                    if(usertype == 1)
                    {
                        Intent dashBoard = new Intent(MainActivity.this, DashBoard.class);
                        startActivity(dashBoard);
                    }
                    else
                    {
                        Intent dashBoard = new Intent(MainActivity.this, DashBoard_Customer.class);
                        startActivity(dashBoard);
                    }


                } else {
                    progressOverlay.setVisibility(View.GONE);
                    theSignInButton.setVisibility(View.VISIBLE);
                    CommonFunctions.showSimpleAlertDialogue
                            (obj.getString("message"), this);

                }

            } catch (Throwable t) {
                theSignInButton.setVisibility(View.VISIBLE);
                Log.i("Result For Array", t.getLocalizedMessage());
                progressOverlay.setVisibility(View.GONE);
                CommonFunctions.showSimpleAlertDialogue
                        ("Sign in unsuccessfull, please try again later", this);
            }
        }

    }

//    public void hideSoftKeyboard(View view){
//        InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
}
