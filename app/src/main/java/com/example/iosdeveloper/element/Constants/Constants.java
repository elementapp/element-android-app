package com.example.iosdeveloper.element.Constants;

public class Constants {
    public static String baseUrl = "http://api.elementapptest.com";
    public static String imageBaseUrl = "http://admin.elementapptest.com/";
    public static String signup = "/api/Auth/register";
    public static String signIn = "/api/Auth/login";
    public static String getProductCategories = "/api/ProductCategory";
    public static String getProductsInaCategories = "/api/Product/GetProductByCategory/{id}";
    public static String getProductDetails = "/api/Product/{id}";
    public static String addProductToCart = "/api/OrderManagement/addProducttoCart";
    public static String getUsersCart = "/api/OrderManagement/getCartItem/{userId}";
    public static String getUsersCartCount = "/api/OrderManagement/cartItemCount/{userId}";
    public static String updateUsersCartItem = "/api/OrderManagement/updateCartItem";
    public static String deleteUsersCartItem = "/api/OrderManagement/deleteCartItem/{userId}/{productId}";
    public static String getManufacturers = "/api/Manufacturer";
    public static String placeOrderForProducts = "/api/OrderManagement/placeOrder";
    public static String getUsersByType = "/api/Auth/userbyusertype/{userType}";
    public static String getSalesRepDetails = "/api/SalesRep/{id}";
    public static String updateUsersLocation = "/api/Auth/registerUserLocation";
    public static String getSalesRepCloseToMyLocation =  "/api/Auth/salesrepClosetoCustomer";
    public static String getUsersOrder = "/api/OrderManagement/getCustomerOrder/{userId}";
    public static String updateUsersOrder = "/api/OrderManagement/uppdateOrderStatus";
}
