package com.example.iosdeveloper.element.NetworkCalls;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by iosdeveloper on 5/22/18.
 */

public class MakeNetwotkRequest extends AsyncTask<Object, Void, Object> {

    private  NetworkCallBacHAndler networkRequestCallBackHandler;
    public MakeNetwotkRequest (NetworkCallBacHAndler event)
    {
        networkRequestCallBackHandler = event;
    }

    private Context contx;
    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }
    @Override
    protected Object doInBackground(Object... params) {
        contx = (Context) params[0];
        String requestType = (String) params[1];
        String requestParam = (String) params[2];
        String requestUrl = (String) params[3];
        String authRequired = (String) params[4];

        Log.e(TAG, "processing http request in async task");

        if ("get".equals(requestType)) {
            Log.e(TAG, "processing get http request using OkHttp");
            return new ExecuteNetworkRequest().performHttpGetRequestWithResponse(requestUrl, authRequired);
        } else if ("getquery".equals(requestType)) {
         Log.e(TAG, "processing get http request with query parameters using OkHttp");
            return new ExecuteNetworkRequest().performHTTPQueryRequestWithResponse(requestParam, requestUrl, authRequired);
         } else if ("postjson".equals(requestType)) {
         Log.e(TAG, "processing post http request using OkHttp for post");
            return new ExecuteNetworkRequest().performHTTPJSONPostRequestWithResponse(requestUrl,requestParam, authRequired);
         } else if ("posturlencoded".equals(requestType)) {
            Log.e(TAG, "processing post http request using OkHttp url encoded for post");
            return new ExecuteNetworkRequest().performpHttpURLEncodedFromPostRequestWithResponse(requestParam,requestUrl, authRequired);
        }
        else if ("put".equals(requestType)) {
            Log.e(TAG, "processing put http request using OkHttp url encoded for post");
            return new ExecuteNetworkRequest().performHTTPJSONPutRequestWithResponse(requestUrl,requestParam, authRequired);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        //Log.i("The Reault Now", result.toString());

        if (result != null) {
            Log.e(TAG, "populate UI after response from service using OkHttp client");
            //Log.i("Api MakeNetworkRequest", (String) result);

                networkRequestCallBackHandler.networkCallHasBeenCompleted(result);

           // MainActivity.requestExecutedSuccessfully(result);




            //respone.setText((String) result);
        }
        else
        {
            networkRequestCallBackHandler.networkCallHasBeenCompleted("Error retrieving your response");
        }
    }

}
