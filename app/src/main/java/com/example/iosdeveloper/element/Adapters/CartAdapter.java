package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.iosdeveloper.element.Interfaces.CartModificationInterface;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForUtilities;
import com.example.iosdeveloper.element.Models.CartModel;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.ProductModel;
import com.example.iosdeveloper.element.R;
import com.example.iosdeveloper.element.SelectedProducts;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolderForCart> {

    private Context mContext;
    private List<ProductModel> mData;
    private CartModificationInterface selectionCallBack;


    public CartAdapter(Context mContext, List<ProductModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.selectionCallBack = (CartModificationInterface) mContext;
    }


    @NonNull
    @Override
    public CartAdapter.MyViewHolderForCart onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_cart,parent,false);
        return new CartAdapter.MyViewHolderForCart(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartAdapter.MyViewHolderForCart holder, final int position) {

        holder.cartProductname.setText(mData.get(position).getProductName());
        holder.cartProductRef.setText(mData.get(position).getProductRef());
        holder.cartProductamount.setText("N"+mData.get(position).getProductAmount());
        holder.cartQuantity.setText(String.valueOf(mData.get(position).getProductQuantity()));
        if(mData.get(position).getProductimageURL().equals("no image"))
        {
            holder.cartImageView.setImageResource(R.drawable.product_selected);
        }
        else {
            Glide.with(mContext).load(mData.get(position).getProductimageURL()).into(holder.cartImageView);
        }

        holder.increaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int theCurrentQuantity = 0;
                try {
                    theCurrentQuantity = Integer.parseInt(holder.cartQuantity.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                theCurrentQuantity++;
                int theNewPrice = mData.get(position).getProductAmount()*theCurrentQuantity;
                Log.i("New Price", "This is the price "+theNewPrice);
                holder.cartQuantity.setText(String.valueOf(theCurrentQuantity));
                holder.cartProductamount.setText("N"+theNewPrice);
                //selectionCallBack.editUsersCart(mData.get(position),theCurrentQuantity);
            }
        });

        holder.reduceQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCurrentQuantity = 0;
                try {
                    theCurrentQuantity = Integer.parseInt(holder.cartQuantity.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                if(theCurrentQuantity>=1) {
                    theCurrentQuantity--;
                    int theNewPrice = mData.get(position).getProductAmount()*theCurrentQuantity;
                    Log.i("New Price", "This is the price "+theNewPrice);
                    holder.cartQuantity.setText(String.valueOf(theCurrentQuantity));
                    holder.cartProductamount.setText("N"+theNewPrice);
                    //selectionCallBack.editUsersCart(mData.get(position),theCurrentQuantity);
                }



            }
        });

        holder.updateCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectionCallBack.editUsersCart(mData.get(position),Integer.valueOf(holder.cartQuantity.getText().toString()));
            }
        });



        holder.deleteItemButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectionCallBack.editUsersCart(mData.get(position),0);

            }
        });

    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForCart extends RecyclerView.ViewHolder{

        TextView cartProductname;
        TextView cartProductRef;
        TextView cartProductamount;
        TextView cartQuantity;
        CardView cardView;
        ImageView cartImageView;
        Button increaseQuantity;
        Button reduceQuantity;
        Button updateCart;
        ImageView deleteItemButon;

        public MyViewHolderForCart(View itemView) {
            super(itemView);

            cartProductname = (TextView) itemView.findViewById(R.id.cartdrugName);
            cartProductRef = (TextView) itemView.findViewById(R.id.cartdrugref);
            cartProductamount = (TextView) itemView.findViewById(R.id.cartAmount);
            cardView = (CardView) itemView.findViewById(R.id.cardview_cart_item);
            increaseQuantity = (Button) itemView.findViewById(R.id.addQuantity);
            reduceQuantity = (Button) itemView.findViewById(R.id.removeQuantity);
            deleteItemButon = (ImageView) itemView.findViewById(R.id.deleteCartItemButton);
            cartQuantity = (TextView) itemView.findViewById(R.id.cartQuantity);
            cartImageView = (ImageView) itemView.findViewById(R.id.cartproductimage);
            updateCart = (Button) itemView.findViewById(R.id.updateCart);


        }
    }
}
