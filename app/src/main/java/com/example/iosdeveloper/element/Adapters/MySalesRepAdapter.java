package com.example.iosdeveloper.element.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.iosdeveloper.element.Customer_SalesMan_Selection;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForDeleteAndEditDetails;
import com.example.iosdeveloper.element.Models.MySalesRepModel;
import com.example.iosdeveloper.element.R;
import com.example.iosdeveloper.element.SalesRepDetails;
import com.example.iosdeveloper.element.ShoppingCart;

import java.util.List;

public class MySalesRepAdapter extends RecyclerView.Adapter<MySalesRepAdapter.MyViewHolderForMySalesRep>{

    private Context mContext;
    private List<MySalesRepModel> mData;
    private SelectionCallBackForDeleteAndEditDetails selectionCallBack;


    public MySalesRepAdapter(Context mContext, List<MySalesRepModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.selectionCallBack = (SelectionCallBackForDeleteAndEditDetails) mContext;
    }

    @NonNull
    @Override
    public MySalesRepAdapter.MyViewHolderForMySalesRep onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.card_view_my_salesrep,parent,false);
        return new MySalesRepAdapter.MyViewHolderForMySalesRep(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final MySalesRepAdapter.MyViewHolderForMySalesRep holder, final int position) {

        holder.repName.setText(mData.get(position).getRepName());
        holder.repID.setText(mData.get(position).getRepId());

        holder.selectSalesRep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**Log.i("see Sales Rep", "We want to view my own sales rep");
                Intent salesReps = new Intent(mContext, SalesRepDetails.class);
                salesReps.putExtra("salesrepID", mData.get(position).getRepId());
                mContext.startActivity(salesReps);**/

                selectionCallBack.selectCustomerAndCreateOrder(mData.get(position));


            }
        });

    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static  class MyViewHolderForMySalesRep extends RecyclerView.ViewHolder{

        TextView repName;
        TextView repID;
        CardView cardView;
        Button selectSalesRep;

        public MyViewHolderForMySalesRep(View itemView) {
            super(itemView);

            repID = (TextView) itemView.findViewById(R.id.myrepid);
            repName = (TextView) itemView.findViewById(R.id.myrepname);
            cardView = (CardView) itemView.findViewById(R.id.cardview_mysalesRep);
            selectSalesRep = (Button) itemView.findViewById(R.id.my_salesrep_select);


        }
    }


}
