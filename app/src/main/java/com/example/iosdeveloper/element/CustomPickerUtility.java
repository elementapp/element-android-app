package com.example.iosdeveloper.element;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.iosdeveloper.element.Adapters.CustomPickerAdapter;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForUtilities;
import com.example.iosdeveloper.element.Models.CustomPickerModel;
import com.example.iosdeveloper.element.Models.CustomerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CustomPickerUtility extends AppCompatActivity implements SelectionCallBackForUtilities {
    List<CustomPickerModel> lstItems;
    List<JSONObject> lstItemsJSON;
    RecyclerView myrv;
    Intent theUtilityIntent;
    String theTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_picker_utility);
        myrv = (RecyclerView) findViewById(R.id.recycler_utility);
        theUtilityIntent = getIntent();
        lstItems = new ArrayList<>();
        setUpRecyclerForUtility(theUtilityIntent.getStringExtra("theutility"));
        theTitle = theUtilityIntent.getStringExtra("label");
    }

    public void setUpRecyclerForUtility(String theUtilityValue)
    {
        try {
            JSONArray utilityCollection = new JSONArray(theUtilityValue);
            for(int i=0;i<utilityCollection.length();i++)
            {
                JSONObject theValue = utilityCollection.getJSONObject(i);
                String theItemName = theValue.getString("name");
                String theid = theValue.getString("idValue");
                lstItems.add(new CustomPickerModel(theItemName,theid));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CustomPickerAdapter myAdapter = new CustomPickerAdapter(this, lstItems);
        myrv.setLayoutManager(new GridLayoutManager(this,1));
        myrv.setAdapter(myAdapter);
    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void selectionHasBeenDone (Object theResult) {
        CustomPickerModel theUtility = (CustomPickerModel) theResult;
        String theName = theUtility.getItemName();
        String theId = theUtility.getItemId();
        Log.i("TheSelected Value", theName + " " + theId);
        if(theTitle.equals("signup"))
        {
           SignUPActivity.theSelectedUserType = theName;
           SignUPActivity.theSelectedUserTypeId = theId;
        }
        finish();
    }
}
