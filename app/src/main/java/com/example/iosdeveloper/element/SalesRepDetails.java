package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Models.MySalesRepModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SalesRepDetails extends AppCompatActivity implements NetworkCallBacHAndler {
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    public static MySalesRepModel theSalesRep;
    int theOrderId;
    int theSalesRepID;
    Intent theUtilityIntent;
    boolean isGetMySalesMenDetails;
    boolean isCreateOrder;
    TextView address;
    TextView email;
    TextView phoneNumberText;
    TextView salesRepId;
    TextView theName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_rep_details);
        theUtilityIntent = getIntent();
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        theSalesRepID = theUtilityIntent.getIntExtra("salesrepID",0000);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        address = (TextView) findViewById(R.id.addressValue);
        email = (TextView) findViewById(R.id.emailValue);
        phoneNumberText = (TextView) findViewById(R.id.phoneValue);
        salesRepId = (TextView) findViewById(R.id.repid);
        theName = (TextView) findViewById(R.id.repName);
        //MySalesRepModel theSalesRep = (MySalesRepModel) theUtilityIntent.getStringExtra("selectedSalesRep");
        setUPUIWithDetails(theSalesRep);
        //getSalesRepDetails();

    }

    void setUPUIWithDetails(MySalesRepModel theRep)
    {
        String name = theRep.getRepName();
        int salesRepIDValue = theRep.getRepIdReal();
        String emailValue = theRep.getRepemail();
        String phoneNumber = theRep.getRepId();
        if (name == null || name.isEmpty()
                || name.equalsIgnoreCase("null")) {
            theName.setText("Not Available");
        }
        else
        {
            theName.setText(name);
        }
        if (emailValue == null || emailValue.isEmpty()
                || emailValue.equalsIgnoreCase("null")) {
            email.setText("Not Available");
        }
        else
        {
            email.setText(emailValue);
        }
        salesRepId.setText("Sales Rep ID: "+salesRepIDValue);
        if (phoneNumber == null || phoneNumber.isEmpty()
                || phoneNumber.equalsIgnoreCase("null")) {
            phoneNumberText.setText("Not Available");
        }
        else
        {
            phoneNumberText.setText(phoneNumber);
        }


    }

    void getSalesRepDetails()
    {
        progressOverlay.setVisibility(View.VISIBLE);
        isGetMySalesMenDetails = true;
        String theUrl = Constants.baseUrl + Constants.getSalesRepDetails.replace("{id}",String.valueOf(theSalesRepID));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);

    }


    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void placeRequest(View view)
    {
        Intent paymentMethod = new Intent(SalesRepDetails.this, PaymentMethods.class);
        paymentMethod.putExtra("salesrepID", theSalesRepID);
        paymentMethod.putExtra("orderid", theOrderId);
        this.startActivity(paymentMethod);
        /**isCreateOrder = true;
        Log.i("Log out", "We want to log out");
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("orderId", theOrderId);
            parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("salesRepId",theSalesRepID);
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.placeOrderForProducts;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }**/

    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            if(isGetMySalesMenDetails)
            {
                isGetMySalesMenDetails = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        JSONObject theItem = obj.getJSONObject("message");
                        String name = theItem.getString("salesRepName");
                        int salesRepIDValue = theItem.getInt("salesRepId");
                        String emailValue = theItem.getString("emailAddress");
                        String phoneNumber = theItem.getString("phoneNumber");
                        if (name == null || name.isEmpty()
                                || name.equalsIgnoreCase("null")) {
                            theName.setText("Not Available");
                        }
                        else
                        {
                            theName.setText(name);
                        }
                        if (emailValue == null || emailValue.isEmpty()
                                || emailValue.equalsIgnoreCase("null")) {
                            email.setText("Not Available");
                        }
                        else
                        {
                            email.setText(emailValue);
                        }
                        salesRepId.setText("Sales Rep ID: "+salesRepIDValue);
                        if (phoneNumber == null || phoneNumber.isEmpty()
                                || phoneNumber.equalsIgnoreCase("null")) {
                            phoneNumberText.setText("Not Available");
                        }
                        else
                        {
                            phoneNumberText.setText(phoneNumber);
                        }

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
            else if(isCreateOrder)
            {
                isCreateOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        Intent theSelection = new Intent(SalesRepDetails.this, TransactionStatus.class);
                        theSelection.putExtra("labelValue", "Order Created");
                        theSelection.putExtra("descriptionValue", obj.getString("message"));
                        startActivity(theSelection);

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                (obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
        }

    }
}
