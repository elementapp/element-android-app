package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONException;
import org.json.JSONObject;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;

public class CardDetails extends AppCompatActivity implements NetworkCallBacHAndler{
    int theSelectedPaymentMethod;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    int theOrderId;
    int theSalesRepID;
    Intent theUtilityIntent;
    boolean isCreateOrder;
    boolean isValidateOrder;
    EditText theCardNumber;
    EditText theCardExpiry;
    EditText theCardCVV;
    String updateString;
    Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        theUtilityIntent = getIntent();
        theOrderId = theUtilityIntent.getIntExtra("orderid",0000);
        theSalesRepID = theUtilityIntent.getIntExtra("salesrepID",0000);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theCardNumber = (EditText) findViewById(R.id.cardNumber);
        theCardExpiry = (EditText) findViewById(R.id.cardExpiry);
        theCardCVV =  (EditText) findViewById(R.id.cardCVV);
    }

    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void placeRequest(View view)
    {
        if(theCardNumber.getText().toString().equals(""))
        {
           CommonFunctions.showSimpleAlertDialogue("Enter your card number", CardDetails.this);
        }
        else if(theCardExpiry.getText().toString().equals(""))
        {
            CommonFunctions.showSimpleAlertDialogue("Enter your card expiry date seperated by /", CardDetails.this);
        }
        else if(theCardCVV.getText().toString().equals(""))
        {
            CommonFunctions.showSimpleAlertDialogue("Enter your card CVV. The three digit number behind your card", CardDetails.this);
        }
        else
        {
            String theCardExpiryValue = theCardExpiry.getText().toString();
            String[] theResult = theCardExpiryValue.split("/");
            if(theResult.length>0 && theResult.length<2)
            {
                CommonFunctions.showSimpleAlertDialogue("Enter your card expiry month and year, with the month and year seperated by /", CardDetails.this);
            }
            else
            {
                card = new Card(theCardNumber.getText().toString(),Integer.valueOf(theResult[0]),Integer.valueOf(theResult[1]),theCardCVV.getText().toString());
                if(card.isValid())
                {
                    Log.i("Card Details", "Number "+card.getNumber()+" Expiry Monty "+card.getExpiryMonth()+" Expiry Year "+card.getExpiryYear()+" CVV "+card.getCvc());
                    placeOrder();
                    //chargeTheCard();

                }
                else
                {
                    CommonFunctions.showSimpleAlertDialogue("Please enter valid card details", CardDetails.this);
                }
            }
        }

            //placeOrder();
    }

    void placeOrder()
    {
        isCreateOrder = true;
        Log.i("Log out", "We want to log out");
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("orderId", theOrderId);
            parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("salesRepId",theSalesRepID);
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.placeOrderForProducts;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        }
        else
        {
            if(isCreateOrder)
            {
                isCreateOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        chargeTheCard(String.valueOf(obj.getInt("message")));
                            /**Intent theSelection = new Intent(CardDetails.this, TransactionStatus.class);
                            theSelection.putExtra("labelValue", "Order Created");
                            theSelection.putExtra("descriptionValue", obj.getString("message"));
                            startActivity(theSelection);**/

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue("Error Processing Your Transaction\nReason:"+obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error processing your request. Try again at some other time", this);
                }

            }
            if(isValidateOrder)
            {
                isValidateOrder = false;
                try {
                    JSONObject obj = new JSONObject(res);
                    String theResponseCode = obj.getString("status");
                    if (theResponseCode.equals("success")) {
                        progressOverlay.setVisibility(View.GONE);
                        String theMessage = "";
                        if(updateString.equals("Paid"))
                        {
                           theMessage = "Order paid for and updated successfully";
                        }
                        else
                        {
                            theMessage = "Order payment was not successful, money will be collected on delivery";
                        }
                        Intent theSelection = new Intent(CardDetails.this, TransactionStatus.class);
                         theSelection.putExtra("labelValue", "Order Created");
                         theSelection.putExtra("descriptionValue", theMessage);
                         startActivity(theSelection);

                    }
                    else
                    {
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue("Error Updating Your Transaction\nReason:"+obj.getString("message"), this);
                    }
                }
                catch (Throwable t) {
                    Log.i("Result For Array", t.getLocalizedMessage());
                    progressOverlay.setVisibility(View.GONE);
                    CommonFunctions.showSimpleAlertDialogue
                            ("Error updating your transaction. Try again at some other time", this);
                }

            }
        }

    }


    void chargeTheCard(final String theReference)
    {
        progressOverlay.setVisibility(View.VISIBLE);
        Charge charge = new Charge();
        charge.setAmount(20000);
        charge.setEmail("iehioze@gmail.com");
        charge.setReference(theReference);
        charge.setCard(card);

        PaystackSdk.chargeCard(CardDetails.this, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                progressOverlay.setVisibility(View.GONE);
                Log.i("Success Transaction", transaction.toString());
                validateSuccessfullTransaction(theReference, "Paid");

            }

            @Override
            public void beforeValidate(Transaction transaction) {
                progressOverlay.setVisibility(View.GONE);
                Log.i("Validate Transaction", transaction.toString());
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                //handle error here
                progressOverlay.setVisibility(View.GONE);
                Log.i("Error Transaction", error.getLocalizedMessage());
                validateSuccessfullTransaction(theReference, "Failed");
                CommonFunctions.showSimpleAlertDialogue
                        ("Error With Payment \nReason: "+error.getLocalizedMessage(), CardDetails.this);
            }

        });
    }

    void validateSuccessfullTransaction(String thePaymentReference, String theStatus)
    {
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            isValidateOrder = true;
            updateString = theStatus;
            parameters.put("orderId", thePaymentReference);
            //parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("orderStatus",theStatus);
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.updateUsersOrder;
            Log.i("JSON String", "this is the url " + theUrl + " and the parameters " + theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    }


