package com.example.iosdeveloper.element.Models;

public class WalletHistorySalesManModel {

    private String itemName;
    private String itemDate;
    private String itemAmount;
    private int transactionType;

    public WalletHistorySalesManModel(String itemName, String itemDate, String itemAmount, int transactionType) {
        this.itemName = itemName;
        this.itemDate = itemDate;
        this.itemAmount = itemAmount;
        this.transactionType = transactionType;
    }

    public String getItemName() {
        return itemName;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public String getItemDate() {
        return itemDate;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setItemDate(String itemDate) {
        this.itemDate = itemDate;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }
}
