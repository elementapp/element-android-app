package com.example.iosdeveloper.element.Interfaces;

public interface SelectionCallBackForDeleteAndEditDetails {

    void editSelectedCustomerDetails(Object theSelection, int thePosition);
    void deleteSelectedCustomerDetails(Object theSelection, int thePosition);
    void selectCustomerAndCreateOrder(Object theSelection);
    void selectNearbySalesRepAndCreateOrder(Object theSelection);
}
