package com.example.iosdeveloper.element.CommonFunctions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.DisplayMetrics;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CommonFunctions {

    public static void showSimpleAlertDialogue(String theMessage, Activity theActivity){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(theActivity);
        alertDialogBuilder.setMessage(theMessage);

        alertDialogBuilder.setNegativeButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //theActivity.finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public static float convertDPToPixels(int dp, Activity theActivity) {
        DisplayMetrics metrics = new DisplayMetrics();
        theActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;
        return dp * logicalDensity;
    }

    /**
     * add days to date in java
     * @param date
     * @param days
     * @return
     */
    public static Date addDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    /**
     * subtract days to date in java
     * @param date
     * @param days
     * @return
     */
    public static Date subtractDays(Date date, int days) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DATE, -days);

        return cal.getTime();
    }
}
