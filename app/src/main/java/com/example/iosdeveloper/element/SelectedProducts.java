package com.example.iosdeveloper.element;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.example.iosdeveloper.element.Adapters.CatalogAdapters;
import com.example.iosdeveloper.element.Adapters.SelectedProductAdapter;
import com.example.iosdeveloper.element.CommonFunctions.CommonFunctions;
import com.example.iosdeveloper.element.Constants.Constants;
import com.example.iosdeveloper.element.Interfaces.SelectionCallBackForUtilities;
import com.example.iosdeveloper.element.Models.CatlogueModel;
import com.example.iosdeveloper.element.Models.CustomPickerModel;
import com.example.iosdeveloper.element.Models.ProductModel;
import com.example.iosdeveloper.element.NetworkCalls.MakeNetwotkRequest;
import com.example.iosdeveloper.element.NetworkCalls.NetworkCallBacHAndler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SelectedProducts extends AppCompatActivity implements NetworkCallBacHAndler, SelectionCallBackForUtilities {
    List<ProductModel> lstProducts;
    List<ProductModel> lstCartProducts;
    RecyclerView myrv;
    String theAccessToken;
    SharedPreferences preferences;
    static View progressOverlay;
    String theCatID;
    Intent theUtilityIntent;
    boolean isAddProductToCart;
    boolean isGetUsersCartCount;
    private SwipeRefreshLayout swipeLayout;
    TextView theCartDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_products);
        myrv = (RecyclerView) findViewById(R.id.recycler_selected_products);
        theCartDisplay = (TextView) findViewById(R.id.cartCountLabel);
        progressOverlay = findViewById(R.id.progress_overlay);
        preferences = this.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE);
        theAccessToken = preferences.getString("accesstoken", "");
        theUtilityIntent = getIntent();
        theCatID = String.valueOf(theUtilityIntent.getIntExtra("categoryid", 500)) ;
        getTheProductsInACategory(theCatID);
        lstCartProducts = new ArrayList<>();
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        //getUsersCartCount();

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i("refresh now", "we are swipping to refresh");
                getTheProductsInACategory(theCatID);
            }
        });
    }


    public void performBackOperation(View view)
    {
        Log.i("Log out", "We want to log out");
        finish();
    }

    public void proceedToCartForCheckOut(View view)
    {
        Log.i("checkout", "Cart Item count is "+theCartDisplay.getText().toString());
        if(theCartDisplay.getText().toString() == "0" || Integer.valueOf(theCartDisplay.getText().toString()) == 0 )
        {
          CommonFunctions.showSimpleAlertDialogue("You currently do not have any product in your cart. " +
                  "Select a product to add it to your cart",this);
        }
        else
        {
            //ShoppingCart.lstProductsForCart = lstCartProducts;
            //String theValue = lstCartProducts.toString();
            Intent intent = new Intent(SelectedProducts.this,ShoppingCart.class);
            //intent.putExtra("cartproducts", theValue);
            //intent.p
            this.startActivity(intent);
        }
    }

    void getTheProductsInACategory(String theCatID)
    {
        Log.i("the cat id", theCatID);
        progressOverlay.setVisibility(View.VISIBLE);
        String theUrl = Constants.baseUrl + Constants.getProductsInaCategories.replace("{id}",theCatID);
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);
    }

    void setUpRecycler()
    {
        getUsersCartCount();
        SelectedProductAdapter myAdapter = new SelectedProductAdapter(this, lstProducts);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);
        ViewTreeObserver viewTreeObserver = myrv.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                calculateSize();
            }
        });
    }

    private static final int sColumnWidth = 165; // assume cell width of 145dp
    private void calculateSize() {
        int spanCount = (int) Math.floor(myrv.getWidth() / CommonFunctions.convertDPToPixels(sColumnWidth, this));
        Log.i("Span Count", "Count is + "+spanCount);
        ((GridLayoutManager) myrv.getLayoutManager()).setSpanCount(spanCount);
    }


    public void networkCallHasBeenCompleted (Object theResult) {
        Log.i("Api Request Result", (String) theResult);
        if(swipeLayout.isRefreshing()) {
            swipeLayout.setRefreshing(false);
        }
        String res = (String) theResult;
        if (theResult == "Error retrieving your response") {
            progressOverlay.setVisibility(View.GONE);
            CommonFunctions.showSimpleAlertDialogue("Your request could not be completed at this time, please try again",
                    this);
        } else
            {
                if(isAddProductToCart)
                {
                    Log.i("Add To Cart Response", (String) theResult);
                    isAddProductToCart = false;
                    try {
                        JSONObject obj = new JSONObject(res);
                        String theResponseCode = obj.getString("status");
                        if (theResponseCode.equals("success")) {
                            progressOverlay.setVisibility(View.GONE);
                            JSONArray theProducts = obj.getJSONArray("message");
                            JSONObject theFirstObject = theProducts.getJSONObject(0);
                            int theNumberInCart = theFirstObject.getInt("itemCount");
                            //if(theNumberInCart > 0) {
                                theCartDisplay.setText(String.valueOf(theNumberInCart));
                                CommonFunctions.showSimpleAlertDialogue
                                        ("Product added to cart successfully", this);
                           // }
                        }
                        else
                        {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    (obj.getString("message"), this);
                        }
                    }
                    catch (Throwable t) {
                        Log.i("Result For Array", t.getLocalizedMessage());
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                ("Products could not be added to cart at this time. Try again at some other time", this);
                    }

                }
                else if(isGetUsersCartCount)
                {
                    isGetUsersCartCount = false;
                    try {
                        JSONObject obj = new JSONObject(res);
                        String theResponseCode = obj.getString("status");
                        if (theResponseCode.equals("success")) {
                            int theCartCount = obj.getInt("message");
                            theCartDisplay.setText(String.valueOf(theCartCount));
                        }
                    }
                    catch (Throwable t) {
                        Log.i("Result For Array", t.getLocalizedMessage());

                    }


                }
                else {
                    try {
                        JSONObject obj = new JSONObject(res);
                        String theResponseCode = obj.getString("status");
                        if (theResponseCode.equals("success")) {
                            JSONArray theProducts = obj.getJSONArray("message");
                            if (theProducts.length() > 0) {
                                progressOverlay.setVisibility(View.GONE);
                                lstProducts = new ArrayList<>();
                                for (int i = 0; i < theProducts.length(); i++) {
                                    JSONObject theCategoryItem = theProducts.getJSONObject(i);
                                    String name = theCategoryItem.getString("productName");
                                    int theProductId = theCategoryItem.getInt("productId");
                                    String productImage = theCategoryItem.getString("productImage");
                                    if (productImage == null || productImage.isEmpty()
                                            || productImage.equalsIgnoreCase("null")
                                            || productImage.equals("string")
                                            || productImage.contains("~")) {
                                        productImage = "no image";
                                    } else {
                                        productImage = Constants.imageBaseUrl + productImage;
                                    }
                                    String theProductRef = theCategoryItem.getString("referenceNumber");
                                    if (theProductRef == null || theProductRef.isEmpty()
                                            || theProductRef.equalsIgnoreCase("null")) {
                                        theProductRef = "000000";
                                    }
                                    int theAmount = theCategoryItem.getInt("price");
                                    lstProducts.add(new ProductModel(productImage, name, "REF. " + String.valueOf(theProductRef),
                                            theAmount, theProductId,0));
                                }
                                setUpRecycler();
                            } else {
                                progressOverlay.setVisibility(View.GONE);
                                CommonFunctions.showSimpleAlertDialogue
                                        ("Products currently not available. Check back later", this);
                            }
                        } else {
                            progressOverlay.setVisibility(View.GONE);
                            CommonFunctions.showSimpleAlertDialogue
                                    (obj.getString("message"), this);
                        }

                    } catch (Throwable t) {
                        Log.i("Result For Array", t.getLocalizedMessage());
                        progressOverlay.setVisibility(View.GONE);
                        CommonFunctions.showSimpleAlertDialogue
                                ("Products currently not available, please try later", this);
                    }
                }

        }

    }

    public void selectionHasBeenDone (Object theResult) {
        isAddProductToCart = true;
        ProductModel theProduct = (ProductModel) theResult;
        String theJsonElement = "";
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("ProductId", theProduct.getProductID());
            parameters.put("userId", preferences.getString("userid", ""));
            parameters.put("userType", preferences.getInt("usertype", 00000));
            parameters.put("ProductQuantity", 1);
            theJsonElement = parameters.toString();
            progressOverlay.setVisibility(View.VISIBLE);
            String theUrl = Constants.baseUrl + Constants.addProductToCart;
            Log.i("JSON String", "this is the url " + theUrl+" and the parameters "+theJsonElement);
            new MakeNetwotkRequest(this).execute(this, "postjson", theJsonElement, theUrl, "");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       /**if(!lstCartProducts.contains(theProduct))
       {
         lstCartProducts.add(theProduct);
       }**/

    }

    void getUsersCartCount()
    {
        isGetUsersCartCount = true;
        String theUrl = Constants.baseUrl + Constants.getUsersCartCount.replace("{userId}",preferences.getString("userid", ""));
        Log.i("JSON String", "this is the url " + theUrl);
        new MakeNetwotkRequest(this).execute(this, "get", "", theUrl,theAccessToken);
    }


}
